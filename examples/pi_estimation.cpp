#include <rngpp/splitmix64.hpp>

#include <iostream>
#include <random>

int main() {
  const int iterations = 100000000;
  int points_in_circle = 0;

  // random_device generates non-deterministic 32-bit numbers
  std::random_device rd;
  // The internal state consists of a single 64-bit integer, so we call
  // random_device twice to generate enough entropy.
  std::seed_seq sequence{rd(), rd()};
  // Seed our splitmix64 generator with the seed_sequence above.
  rngpp::splitmix64 generator(sequence);

  std::uniform_real_distribution<double> real_distribution(0, 1);

  for (int i = 0; i < iterations; i++) {
    const double x = real_distribution(generator);
    const double y = real_distribution(generator);
    const double square_norm = x * x + y * y;

    // Count the number of two-dimensional points on [0, 1]^2 which fall in the
    // inscribed circle.
    if (square_norm <= 1) {
      ++points_in_circle;
    }
  }

  std::cout << "An estimate of pi is "
            << static_cast<double>(4 * points_in_circle) /
                   static_cast<double>(iterations)
            << ".\n";
}
