#include <rngpp/xoshiro.hpp>

#include <iostream>
#include <random>

int main() {
  // Initialize a xoshiro32 generator with seed 42.
  rngpp::xoshiro32 generator(42);

  std::uniform_int_distribution<int> int_distribution(0, 10);

  for (int i = 0; i < 10; ++i) {
    std::cout << "Here is an integer distributed uniformly on {0, ..., 10}: "
              << int_distribution(generator) << ".\n";
  }
}
