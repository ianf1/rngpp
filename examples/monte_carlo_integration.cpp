#include <rngpp/sfc.hpp>

#include <cmath>
#include <iostream>
#include <random>

// This example follows
// https://en.wikipedia.org/wiki/Monte_Carlo_integration#Wolfram_Mathematica_example.

int main() {
  const int iterations = 10000000;

  // random_device generates non-deterministic 32-bit numbers
  std::random_device rd;
  // The internal state consists of 4 64-bit integers, so we call random_device
  // 8 times to generate enough entropy.
  std::seed_seq sequence{rd(), rd(), rd(), rd(), rd(), rd(), rd(), rd()};
  // Seed our sfc64 generator with the seed_sequence above.
  rngpp::sfc64 generator(sequence);

  std::uniform_real_distribution<double> real_distribution(0.8, 3);

  double average = 0;
  for (int i = 0; i < iterations; i++) {
    const double u = real_distribution(generator);
    // The coefficient normalizes the integrand to a probability density
    // function.
    average += (real_distribution.b() - real_distribution.a()) /
               (1. + std::sinh(2 * u) * std::log(u) * std::log(u));
  }
  average /= double(iterations);

  std::cout
      << "The approximate value of the integral between "
      << real_distribution.a() << " and " << real_distribution.b()
      << " of the function defined by f(x) := 1 / (1 + sinh(2u) * log(u)^2) is "
      << average << ". The correct value is 0.67684.\n";
}
