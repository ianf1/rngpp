add_executable(basic basic.cpp)
target_link_libraries(basic rngpp)

add_executable(pi_estimation pi_estimation.cpp)
target_link_libraries(pi_estimation rngpp)

add_executable(monte_carlo_integration monte_carlo_integration.cpp)
target_link_libraries(monte_carlo_integration rngpp)

set_target_properties(basic pi_estimation monte_carlo_integration
    PROPERTIES
    RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/examples"
)