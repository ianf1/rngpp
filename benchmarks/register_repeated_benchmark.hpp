//! @file
// Copyright Ian Flint 2019, distributed under the MIT License.

#ifndef RNGPP_INCLUDE_REPEATED_BENCHMARK
#define RNGPP_INCLUDE_REPEATED_BENCHMARK

#include <benchmark/benchmark.h>

#include <utility>

namespace rngpp {
namespace benchmarks {

template <typename S, typename F, typename... Args>
inline auto register_repeated_benchmark(const S& name, F&& f,
                                        double time_per_benchmark_in_sec,
                                        int different_runs, Args&&... args) {
  const double time_per_run{time_per_benchmark_in_sec /
                            static_cast<double>(different_runs)};

  auto benchmark = benchmark::RegisterBenchmark(name, std::forward<F>(f),
                                                std::forward<Args>(args)...);
  benchmark->MinTime(time_per_run);
  benchmark->Repetitions(different_runs);
  benchmark->DisplayAggregatesOnly(true);
  return benchmark;
}

}  // namespace benchmarks
}  // namespace rngpp

#endif  // RNGPP_INCLUDE_REPEATED_BENCHMARK
