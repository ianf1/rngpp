//! @file
// Copyright Ian Flint 2019, distributed under the MIT License.

#include "../misc/reference_implementations/jsf32.hpp"
#include "../misc/reference_implementations/jsf64.hpp"
#include "../misc/reference_implementations/mulberry32.hpp"
#include "../misc/reference_implementations/sfc32.hpp"
#include "../misc/reference_implementations/sfc64.hpp"
#include "../misc/reference_implementations/splitmix64.hpp"
#include "../misc/reference_implementations/xorshift32.hpp"
#include "../misc/reference_implementations/xorshift64.hpp"
#include "../misc/reference_implementations/xorshiftstar32.hpp"
#include "../misc/reference_implementations/xorshiftstar64.hpp"
#include "../misc/reference_implementations/xoshiroplus32.hpp"
#include "../misc/reference_implementations/xoshiroplus64.hpp"
#include "../misc/reference_implementations/xoshirostarstar32.hpp"
#include "../misc/reference_implementations/xoshirostarstar64.hpp"

#include "register_repeated_benchmark.hpp"

#include <benchmark/benchmark.h>
#include <pcg_random.hpp>

#include <rngpp/jsf.hpp>
#include <rngpp/mulberry32.hpp>
#include <rngpp/sfc.hpp>
#include <rngpp/splitmix64.hpp>
#include <rngpp/xorshift.hpp>
#include <rngpp/xoshiro.hpp>

#include <array>
#include <cstdint>
#include <random>

template <typename Generator>
static void BM_draw_from_generator_implementation(benchmark::State& state,
                                                  Generator generator) {
  for (auto _ : state) {
    benchmark::DoNotOptimize(generator());
  }
}

template <typename Generator>
static void BM_draw_from_generator(benchmark::State& state) {
  // Note: If one instead seeds with a specific value, then xoshiro and
  // sometimes jsf are exceedingly slow with GCC, because these generators have
  // a lengthy seeding procedure, which for some reason confuses the compiler
  // when timing the generation function.
  Generator generator{};
  BM_draw_from_generator_implementation(state, generator);
}

static void BM_reference_jsf32(benchmark::State& state) {
  rngpp::detail_jsf32::ranctx x{};
  rngpp::detail_jsf32::raninit(x, rngpp::jsf32::default_seed);

  for (auto _ : state) {
    benchmark::DoNotOptimize(rngpp::detail_jsf32::next(x));
  }
}

static void BM_reference_jsf64(benchmark::State& state) {
  rngpp::detail_jsf64::ranctx x{};
  rngpp::detail_jsf64::raninit(x, rngpp::jsf64::default_seed);

  for (auto _ : state) {
    benchmark::DoNotOptimize(rngpp::detail_jsf64::next(x));
  }
}

static void BM_reference_mulberry32(benchmark::State& state) {
  auto x{rngpp::mulberry32::default_seed};

  for (auto _ : state) {
    benchmark::DoNotOptimize(rngpp::detail_mulberry32::next(x));
  }
}

static void BM_reference_sfc32(benchmark::State& state) {
  auto x{rngpp::sfc32::default_seed};
  std::array<std::uint32_t, 4> s{};
  rngpp::detail_sfc32::seed(s, x);

  for (auto _ : state) {
    benchmark::DoNotOptimize(rngpp::detail_sfc32::next(s));
  }
}

static void BM_reference_sfc64(benchmark::State& state) {
  auto x{rngpp::sfc64::default_seed};
  std::array<std::uint64_t, 4> s{};
  rngpp::detail_sfc64::seed(s, x);

  for (auto _ : state) {
    benchmark::DoNotOptimize(rngpp::detail_sfc64::next(s));
  }
}

static void BM_reference_splitmix64(benchmark::State& state) {
  auto x{rngpp::splitmix64::default_seed};

  for (auto _ : state) {
    benchmark::DoNotOptimize(rngpp::detail_splitmix64::next(x));
  }
}

static void BM_reference_xorshiftstar32(benchmark::State& state) {
  auto x{rngpp::xorshiftstar32::default_seed};

  for (auto _ : state) {
    benchmark::DoNotOptimize(rngpp::detail_xorshiftstar32::next(x));
  }
}

static void BM_reference_xorshiftstar64(benchmark::State& state) {
  auto x{rngpp::xorshiftstar64::default_seed};

  for (auto _ : state) {
    benchmark::DoNotOptimize(rngpp::detail_xorshiftstar64::next(x));
  }
}

static void BM_reference_xorshift32(benchmark::State& state) {
  auto x{rngpp::xorshift32::default_seed};

  for (auto _ : state) {
    benchmark::DoNotOptimize(rngpp::detail_xorshift32::next(x));
  }
}

static void BM_reference_xorshift64(benchmark::State& state) {
  auto x{rngpp::xorshift64::default_seed};

  for (auto _ : state) {
    benchmark::DoNotOptimize(rngpp::detail_xorshift64::next(x));
  }
}

static void BM_reference_xoshiroplus32(benchmark::State& state) {
  std::uint32_t s[4] = {2245729415UL, 1025911202UL, 1978641587UL, 1422566965UL};

  for (auto _ : state) {
    benchmark::DoNotOptimize(rngpp::detail_xoshiroplus32::next(s));
  }
}

static void BM_reference_xoshiroplus64(benchmark::State& state) {
  std::uint64_t s[4] = {950540965416224045ULL, 3965333047987623914ULL,
                        245237344089695510ULL, 14507415458101264242ULL};

  for (auto _ : state) {
    benchmark::DoNotOptimize(rngpp::detail_xoshiroplus64::next(s));
  }
}

static void BM_reference_xoshirostarstar32(benchmark::State& state) {
  std::uint32_t s[4] = {2245729415UL, 1025911202UL, 1978641587UL, 1422566965UL};

  for (auto _ : state) {
    benchmark::DoNotOptimize(rngpp::detail_xoshirostarstar32::next(s));
  }
}

static void BM_reference_xoshirostarstar64(benchmark::State& state) {
  std::uint64_t s[4] = {950540965416224045ULL, 3965333047987623914ULL,
                        245237344089695510ULL, 14507415458101264242ULL};

  for (auto _ : state) {
    benchmark::DoNotOptimize(rngpp::detail_xoshirostarstar64::next(s));
  }
}

int main(int argc, char** argv) {
  benchmark::Initialize(&argc, argv);

  constexpr double time_per_benchmark_in_sec{10.0};
  constexpr int different_runs{100};

#define RNGPP_BENCHMARK_DRAW_GENERATOR(GEN)                      \
  rngpp::benchmarks::register_repeated_benchmark(                \
      "Draw from generator " #GEN, &BM_draw_from_generator<GEN>, \
      time_per_benchmark_in_sec, different_runs)

  RNGPP_BENCHMARK_DRAW_GENERATOR(pcg32);
  RNGPP_BENCHMARK_DRAW_GENERATOR(pcg64);

  RNGPP_BENCHMARK_DRAW_GENERATOR(std::mt19937);
  RNGPP_BENCHMARK_DRAW_GENERATOR(std::mt19937_64);

  RNGPP_BENCHMARK_DRAW_GENERATOR(rngpp::jsf32);
  rngpp::benchmarks::register_repeated_benchmark(
      "Draw from reference jsf32", &BM_reference_jsf32,
      time_per_benchmark_in_sec, different_runs);

  RNGPP_BENCHMARK_DRAW_GENERATOR(rngpp::jsf64);
  rngpp::benchmarks::register_repeated_benchmark(
      "Draw from reference jsf64", &BM_reference_jsf64,
      time_per_benchmark_in_sec, different_runs);

  RNGPP_BENCHMARK_DRAW_GENERATOR(rngpp::mulberry32);
  rngpp::benchmarks::register_repeated_benchmark(
      "Draw from reference mulberry32", &BM_reference_mulberry32,
      time_per_benchmark_in_sec, different_runs);

  RNGPP_BENCHMARK_DRAW_GENERATOR(rngpp::sfc32);
  rngpp::benchmarks::register_repeated_benchmark(
      "Draw from reference sfc32", &BM_reference_sfc32,
      time_per_benchmark_in_sec, different_runs);

  RNGPP_BENCHMARK_DRAW_GENERATOR(rngpp::sfc64);
  rngpp::benchmarks::register_repeated_benchmark(
      "Draw from reference sfc64", &BM_reference_sfc64,
      time_per_benchmark_in_sec, different_runs);

  RNGPP_BENCHMARK_DRAW_GENERATOR(rngpp::splitmix64);
  rngpp::benchmarks::register_repeated_benchmark(
      "Draw from reference splitmix64", &BM_reference_splitmix64,
      time_per_benchmark_in_sec, different_runs);

  RNGPP_BENCHMARK_DRAW_GENERATOR(rngpp::xorshift32);
  rngpp::benchmarks::register_repeated_benchmark(
      "Draw from reference xorshift32", &BM_reference_xorshift32,
      time_per_benchmark_in_sec, different_runs);

  RNGPP_BENCHMARK_DRAW_GENERATOR(rngpp::xorshift64);
  rngpp::benchmarks::register_repeated_benchmark(
      "Draw from reference xorshift64", &BM_reference_xorshift64,
      time_per_benchmark_in_sec, different_runs);

  RNGPP_BENCHMARK_DRAW_GENERATOR(rngpp::xorshiftstar32);
  rngpp::benchmarks::register_repeated_benchmark(
      "Draw from reference xorshiftstar32", &BM_reference_xorshiftstar32,
      time_per_benchmark_in_sec, different_runs);

  RNGPP_BENCHMARK_DRAW_GENERATOR(rngpp::xorshiftstar64);
  rngpp::benchmarks::register_repeated_benchmark(
      "Draw from reference xorshiftstar64", &BM_reference_xorshiftstar64,
      time_per_benchmark_in_sec, different_runs);

  RNGPP_BENCHMARK_DRAW_GENERATOR(rngpp::xoshiroplus32);
  rngpp::benchmarks::register_repeated_benchmark(
      "Draw from reference xoshiroplus32", &BM_reference_xoshiroplus32,
      time_per_benchmark_in_sec, different_runs);

  RNGPP_BENCHMARK_DRAW_GENERATOR(rngpp::xoshiroplus64);
  rngpp::benchmarks::register_repeated_benchmark(
      "Draw from reference xoshiroplus64", &BM_reference_xoshiroplus64,
      time_per_benchmark_in_sec, different_runs);
  RNGPP_BENCHMARK_DRAW_GENERATOR(rngpp::xoshirostarstar32);
  rngpp::benchmarks::register_repeated_benchmark(
      "Draw from reference xoshirostarstar32", &BM_reference_xoshirostarstar32,
      time_per_benchmark_in_sec, different_runs);

  RNGPP_BENCHMARK_DRAW_GENERATOR(rngpp::xoshirostarstar64);
  rngpp::benchmarks::register_repeated_benchmark(
      "Draw from reference xoshirostarstar64", &BM_reference_xoshirostarstar64,
      time_per_benchmark_in_sec, different_runs);

  benchmark::RunSpecifiedBenchmarks();
}
