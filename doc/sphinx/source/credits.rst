.. _credits:

Credits
=======

The algorithms underlying the different RNGs in this library are all in the public domain. In particular, you will find the short reference implementations for each RNG in the following places.

* The JSF RNG may be found on `Bob Jenkins' site <http://burtleburtle.net/bob/rand/smallprng.html>`__.
* The Mulberry RNG may be found on `Tommy Ettinger's Github <https://gist.github.com/tommyettinger/46a874533244883189143505d203312c>`__.
* The SFC RNG may be found on the `PractRand site <http://pracrand.sourceforge.net/>`__.
* The SplitMix64 RNG was introduced by Guy L. Steele, Jr., Doug Lea, and Christine H. Flood. A short reference implementation may be found on `Sebastiano Vigna's website <http://xoshiro.di.unimi.it/splitmix64.c>`__.
* The xorshift RNG was proposed by Marsaglia, and some of the variants used here are due to `Sebastiano Vigna <http://vigna.di.unimi.it/papers.php#VigEEMXGS>`__ and `Melissa E. O'Neill <https://gist.github.com/imneme/9b769cefccac1f2bd728596da3a856dd>`__.
* The xoshiro family of RNGs may be found on `Sebastiano Vigna's website <http://xoshiro.di.unimi.it/>`__.

In addition to the above, the `pcg-random <http://www.pcg-random.org/>`__ website has been immensely useful in writing the library.
In particular, `Melissa E. O'Neill <http://www.pcg-random.org/posts/some-prng-implementations.html>`__ has proposed reference implementations for a wide range of RNGs.

A series of helpful comments by `AppleBean <https://www.reddit.com/user/AppleBeam>`__ on a previous version of the library were also very useful.