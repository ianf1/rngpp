.. _benchmarks:

Benchmarks
==========

Using the utility
-----------------

The library provides a benchmarking suite comparing the various RNGs.
It is built by default, or with option ``BENCHMARKS`` when `CMake <https://cmake.org/>`__ is used.

.. warning::
   The benchmarks are in a very controlled setting and the results are very sensitive to small changes in the benchmarked code.
   As such, they are not meant to replace a benchmark of your own use case.
   Instead, an important function of the benchmarks is to enable the comparison of RNG++ with the (often very short) reference implementation.

The binary ``generators`` is located in ``build/benchmarks`` and by default runs all the benchmarks.

.. note:: 

   `Google benchmark <https://github.com/google/benchmark>`__  is used as the underlying benchmarking suite and as such, one may provide arguments to ``generators`` in order to only run certain benchmarks.
   For example, 

   .. code-block:: console

      $ ./generators --benchmark_filter="jsf"

   will execute all benchmarks that involve Bob Jenkins’ Small PRNG.

Results
-------

The utility has been executed on an Intel® i7-6700K CPU running @ 4.2 Ghz, on Linux 64 bit, with GCC 8.3.0 and Clang 8.0.0.
For each RNG, the minimum between the execution time of binaries compiled with Clang and GCC has been taken.
The indicated result is the time taken to generate a single integer by a given RNG.

.. image::
   ../../../misc/gnuplot/output.png
