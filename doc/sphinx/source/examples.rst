.. _examples:

Examples
========

Pi estimation
-------------

The following code is a straightforward application of the `Monte Carlo method <https://en.wikipedia.org/wiki/Monte_Carlo_method>`__:

.. literalinclude:: ../../../examples/pi_estimation.cpp
   :language: cpp

The above code can be compiled by going to the ``/examples/`` subdirectory and typing the following command:

.. code-block:: console

   $ g++ -I../include -std=c++17 -Wall pi_estimation.cpp

On an Intel® i7-6700K CPU running @ 4.2 GHz, the program above compiled with GCC 8.3.0 executes roughly 20% more quickly than the analogous one with ``std::mt19937_64`` in place of ``rngpp::splitmix64``.

Integral estimation
-------------------

The following code illustrates another use of the `Monte Carlo method <https://en.wikipedia.org/wiki/Monte_Carlo_method>`__:

.. literalinclude:: ../../../examples/monte_carlo_integration.cpp
   :language: cpp

The above code can be compiled by going to the ``/examples/`` subdirectory and typing the following command:

.. code-block:: console

   $ g++ -I../include -std=c++17 -Wall monte_carlo_integration.cpp

On an Intel® i7-6700K CPU running @ 4.2 GHz, the program above compiled with GCC 8.3.0 executes roughly 5% more quickly than the analogous one with ``std::mt19937_64`` in place of ``rngpp::splitmix64`` (in this case the generation of uniform samples is not the bottleneck).
