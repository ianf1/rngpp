.. _xoshiro_reference:

Xoshiro generator
=================

*#include <rngpp/xoshiro.hpp>*

Engine
------

.. doxygenclass:: rngpp::xoshiro_engine
   :members:

Convenience typedefs
--------------------

32bit RNGs
~~~~~~~~~~

.. doxygentypedef:: rngpp::xoshiroplus32

.. doxygentypedef:: rngpp::xoshirostarstar32

.. doxygentypedef:: rngpp::xoshiro32

64bit RNGs
~~~~~~~~~~

.. doxygentypedef:: rngpp::xoshiroplus64

.. doxygentypedef:: rngpp::xoshirostarstar64

.. doxygentypedef:: rngpp::xoshiro64
