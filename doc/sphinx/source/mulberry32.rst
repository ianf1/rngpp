.. _mulberry32_reference:

Mulberry32 generator
====================

*#include <rngpp/mulberry32.hpp>*

Engine
------

.. doxygenclass:: rngpp::mulberry32_engine
   :members:

Convenience typedefs
--------------------

.. doxygentypedef:: rngpp::mulberry32
