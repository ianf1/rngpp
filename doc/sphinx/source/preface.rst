.. _preface:

Preface
=======

Supported compilers
-------------------

RNG++ uses a few C++17 features and thus requires a modern compiler. It is known to work on the following setups:

- GCC 7.4.0+ on Linux 32/64 bit;
- Clang 6.0.0+ on Linux 32/64 bit;

and it has also been built and run successfully on:

- Intel® C++ Compiler 19.0.2 on Linux 64 bit;
- Microsoft Visual Studio 2019 16.0 on Windows 7.

The library does not use any platform-specific code so it should in principle also be usable on macOS.

Dependencies
------------

RNG++ does not have any external dependencies.
However, in order to run the tests and benchmarks or build the documentation you will want to install the following optional dependencies:

- `CMake <https://cmake.org/>`__ is used to build the tests and benchmarks;
- `Catch2 <https://github.com/catchorg/Catch2>`__ is the test framework, and has been included as a submodule;
- `Doxygen <http://www.doxygen.nl/>`__, `sphinx <https://www.sphinx-doc.org/en/master/>`__ and `breathe <https://breathe.readthedocs.io/en/latest/>`__ are used to build the documentation;
- `Google benchmark <https://github.com/google/benchmark>`__ is the benchmarking suite used.

Installation
------------

This is a header-only library and as such does not require any installation per se. 
Just point your compiler to the ``/include/`` directory, and then include the header files that you need.

Getting started
---------------

In order to make sure that the setup is working correctly, you may try to compile the following simple code:

.. literalinclude:: ../../../examples/basic.cpp
   :language: cpp

The above code can be compiled by going to the ``/examples/`` subdirectory and typing the following command:

.. code-block:: console

   $ g++ -I../include -std=c++17 -Wall basic.cpp

.. note::
   The generator is seeded with a fixed value, so the compiled program will output the same sequence of numbers on every run.

Using CMake
-----------

In order to use `CMake <https://cmake.org/>`__ to build the various extra features of the library, you may proceed as follows.
Start by obtaining a copy of RNG++, place it in a new directory, say ``rngpp``, and ``cd`` to the directory.
Then, type the following commands into a console:

.. code-block:: console

   $ mkdir build
   $ cd build
   $ cmake .. -DCMAKE_BUILD_TYPE=Release
   $ cmake --build . --target all

.. note:: 

   The tests might be a bit slow to run if the library is built in ``Debug`` mode.

Running the tests
-----------------

The tests are built by default, or with option ``TESTS`` when `CMake <https://cmake.org/>`__ is used.
To run the tests, go to the ``build`` subdirectory and execute the binary ``all_tests``.

.. note:: 

   `Catch2 <https://github.com/catchorg/Catch2>`__ is used and as such, one may provide arguments to ``all_tests`` in order to only execute certain tests.
   For example, 

   .. code-block:: console

      $ ./all_tests [jsf]

   will execute all tests that involve Bob Jenkins’ Small PRNG.

Generators which were not included
----------------------------------

The `PCG <https://en.wikipedia.org/wiki/Permuted_congruential_generator>`__ family of RNGs is notably missing from the library.
This is simply because the `implementation <http://www.pcg-random.org/download.html>`__ given by Melissa E. O'Neill can already be used as a drop-in replacement for RNGs in the C++ standard library.
Although the generators may be obtained from the link above, the PCG RNGs are included as a submodule for ease of access.
They are also included in the benchmarks.

Another RNG which is not included is ChaCha.
This is also because some good C++ implementations already exist (see `Orson Peters' implementation <https://gist.github.com/orlp/32f5d1b631ab092608b1>`__ as well as `Melissa E. O'Neill's variant <https://gist.github.com/imneme/f0fe8877e4deb3f6b9200a17c18bf155>`__).
