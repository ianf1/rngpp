.. _validation_values:

Generation of the validation values
===================================

The library provides small utilities which generate the validation values which are used in the tests to compare our RNGs to the reference implementation.
They are built by default, or with option ``VALIDATION_VALUES`` when `CMake <https://cmake.org/>`__ is used.

The binaries corresponding to all the RNGs are located in ``build/rng_validation_values/``.
Executing any one of the binaries will output the validation values which are used in the tests.
