.. _rng_quality:

Statistical quality of the RNGs
===========================================

Using the utility
-----------------

The library provides small utilities which output a stream of random bits to stdout, and are intented to be used with `PractRand <http://pracrand.sourceforge.net/>`__.
They are built by default, or with option ``QUALITY`` when `CMake <https://cmake.org/>`__ is used.
The basic procedure is the one described by `Melissa E. O'Neill <http://www.pcg-random.org/posts/how-to-test-with-practrand.html>`__.

The binaries corresponding to each of the RNGs are located in ``build/rng_quality/``.

Once `PractRand <http://pracrand.sourceforge.net/>`__ is built, example usage is as follows:

.. code-block:: console

   $ ./jsf32_quality | ./RNG_test stdin32 -multithreaded

Results
-------

The following table represents the amount of random bytes that can be fed into `PractRand <http://pracrand.sourceforge.net/>`__ before a noticable bias is detected in the RNG.

.. role:: green

.. role:: orange

.. role:: red

.. table::
   :class: rows

   +--------------------+-----------------+
   | RNG                | First failure   |
   +====================+=================+
   | jsf32              | :green:`> 8TB`  |
   +--------------------+-----------------+
   | jsf64              | :green:`> 8TB`  |
   +--------------------+-----------------+
   | mulberry32         | :orange:`1GB`   |
   +--------------------+-----------------+
   | mt19937            | :orange:`256GB` |
   +--------------------+-----------------+
   | mt19937_64         | :orange:`512GB` |
   +--------------------+-----------------+
   | sfc32              | :green:`> 8TB`  |
   +--------------------+-----------------+
   | sfc64              | :green:`> 8TB`  |
   +--------------------+-----------------+
   | splitmix64         | :green:`> 8TB`  |
   +--------------------+-----------------+
   | xorshift32         | :red:`64KB`     |
   +--------------------+-----------------+
   | xorshift64         | :red:`64KB`     |
   +--------------------+-----------------+
   | xorshiftstar32     | :green:`> 8TB`  |
   +--------------------+-----------------+
   | xorshiftstar64     | :red:`1MB`      |
   +--------------------+-----------------+
   | xoshiroplus32      | :red:`16MB`     |
   +--------------------+-----------------+
   | xoshiroplus64      | :red:`128MB`    |
   +--------------------+-----------------+
   | xoshirostarstar32  | :green:`> 8TB`  |
   +--------------------+-----------------+
   | xoshirostarstar64  | :green:`> 8TB`  |
   +--------------------+-----------------+

Raw results
-----------

jsf32
~~~~~

.. rubric:: Internal state
.. code-block:: console

   3009285267 2463494095 1547092013 267043580

.. rubric:: Raw results
.. literalinclude:: ../../../misc/statistical_tests_raw_results/jsf32.txt
   :language: console

jsf64
~~~~~

.. rubric:: Internal state
.. code-block:: console

   5749598573436321444 4945953453700098909 8894361242985051590 3237092471147167429

.. rubric:: Raw results
.. literalinclude:: ../../../misc/statistical_tests_raw_results/jsf64.txt
   :language: console

mt19937
~~~~~~~

.. rubric:: Seed
.. code-block:: console

   42

.. rubric:: Raw results
.. literalinclude:: ../../../misc/statistical_tests_raw_results/mt19937.txt
   :language: console

mt19937_64
~~~~~~~~~~

.. rubric:: Seed
.. code-block:: console

   42

.. rubric:: Raw results
.. literalinclude:: ../../../misc/statistical_tests_raw_results/mt19937_64.txt
   :language: console

mulberry32
~~~~~~~~~~

.. rubric:: Internal state
.. code-block:: console

   42

.. rubric:: Raw results
.. literalinclude:: ../../../misc/statistical_tests_raw_results/mulberry32.txt
   :language: console

sfc32
~~~~~

.. rubric:: Internal state
.. code-block:: console

   3692514755 1869228030 2425139185 9

.. rubric:: Raw results
.. literalinclude:: ../../../misc/statistical_tests_raw_results/sfc32.txt
   :language: console

sfc64
~~~~~

.. rubric:: Internal state
.. code-block:: console

   14205098417528888103 18218849306780396024 6934611622550749685 9

.. rubric:: Raw results
.. literalinclude:: ../../../misc/statistical_tests_raw_results/sfc64.txt
   :language: console

splitmix64
~~~~~~~~~~

.. rubric:: Internal state
.. code-block:: console

   42 11400714819323198485

.. rubric:: Raw results
.. literalinclude:: ../../../misc/statistical_tests_raw_results/splitmix64.txt
   :language: console

xorshift32
~~~~~~~~~~~~~~

.. rubric:: Internal state
.. code-block:: console

   42

.. rubric:: Raw results
.. literalinclude:: ../../../misc/statistical_tests_raw_results/xorshift32.txt
   :language: console

xorshift64
~~~~~~~~~~~~~~

.. rubric:: Internal state
.. code-block:: console

   42

.. rubric:: Raw results
.. literalinclude:: ../../../misc/statistical_tests_raw_results/xorshift64.txt
   :language: console

xorshiftstar32
~~~~~~~~~~~~~~

.. rubric:: Internal state
.. code-block:: console

   42

.. rubric:: Raw results
.. literalinclude:: ../../../misc/statistical_tests_raw_results/xorshiftstar32.txt
   :language: console

xorshiftstar64
~~~~~~~~~~~~~~

.. rubric:: Internal state
.. code-block:: console

   42

.. rubric:: Raw results
.. literalinclude:: ../../../misc/statistical_tests_raw_results/xorshiftstar64.txt
   :language: console

xoshiroplus32
~~~~~~~~~~~~~

.. rubric:: Internal state
.. code-block:: console

   2581720956 1925393290 3661312704 2876485805

.. rubric:: Raw results
.. literalinclude:: ../../../misc/statistical_tests_raw_results/xoshiroplus32.txt
   :language: console

xoshiroplus64
~~~~~~~~~~~~~

.. rubric:: Internal state
.. code-block:: console

   13679457532755275413 2949826092126892291 5139283748462763858 6349198060258255764

.. rubric:: Raw results
.. literalinclude:: ../../../misc/statistical_tests_raw_results/xoshiroplus64.txt
   :language: console

xoshirostarstar32
~~~~~~~~~~~~~~~~~

.. rubric:: Internal state
.. code-block:: console

   2581720956 1925393290 3661312704 2876485805

.. rubric:: Raw results
.. literalinclude:: ../../../misc/statistical_tests_raw_results/xoshirostarstar32.txt
   :language: console

xoshirostarstar64
~~~~~~~~~~~~~~~~~

.. rubric:: Internal state
.. code-block:: console

   13679457532755275413 2949826092126892291 5139283748462763858 6349198060258255764

.. rubric:: Raw results
.. literalinclude:: ../../../misc/statistical_tests_raw_results/xoshirostarstar64.txt
   :language: console
