.. _int_splitmix64_reference:

SplitMix64 generator
====================

*#include <rngpp/splitmix64.hpp>*

Engine
------

.. doxygenclass:: rngpp::splitmix64_engine
   :members:

Convenience typedefs
--------------------

.. doxygentypedef:: rngpp::splitmix64
