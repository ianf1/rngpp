.. _xorshift_reference:

Xorshift generator
==================

*#include <rngpp/xorshift.hpp>*

Engine
------

.. doxygenclass:: rngpp::xorshift_engine
   :members:

Convenience typedefs
--------------------

32bit RNGs
~~~~~~~~~~

.. doxygentypedef:: rngpp::xorshift32

.. doxygentypedef:: rngpp::xorshiftstarupperbits32a

.. doxygentypedef:: rngpp::xorshiftstarupperbits32b

.. doxygentypedef:: rngpp::xorshiftstarupperbits32c

.. doxygentypedef:: rngpp::xorshiftstarupperbits32d

.. doxygentypedef:: rngpp::xorshiftstar32

64bit RNGs
~~~~~~~~~~

.. doxygentypedef:: rngpp::xorshift64

.. doxygentypedef:: rngpp::xorshiftstar64
