.. _jsf_reference:

Bob Jenkins' Small PRNG
=======================

*#include <rngpp/jsf.hpp>*

Engine
------

.. doxygenclass:: rngpp::jsf_engine
   :members:

Convenience typedefs
--------------------

32bit RNGs
~~~~~~~~~~

.. doxygentypedef:: rngpp::jsf32_two_rotates

.. doxygentypedef:: rngpp::jsf32_three_rotates

.. doxygentypedef:: rngpp::jsf32

64bit RNGs
~~~~~~~~~~

.. doxygentypedef:: rngpp::jsf64_two_rotates

.. doxygentypedef:: rngpp::jsf64_three_rotates

.. doxygentypedef:: rngpp::jsf64
