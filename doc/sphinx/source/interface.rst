.. _interface:

RNG interface
=============

The generators included in RNG++ all model the `RandomNumberEngine <https://en.cppreference.com/w/cpp/named_req/RandomNumberEngine>`__ concept, and provide a few additional features outlined below.

Given 

- an RNG ``Generator``;
- an integral type ``ResultType`` determined by ``Generator::result_type``;
- an integral type ``StateType`` determined by ``Generator::state_type``;
- a non-const value ``generator`` of type ``Generator``;
- a value ``other_generator`` of type ``Generator``;
- a value ``seed`` of type ``StateType``;
- a `lvalue <https://en.cppreference.com/w/c/language/value_category#Lvalue_expressions>`__ ``sequence`` satisfying `SeedSequence <https://en.cppreference.com/w/cpp/named_req/SeedSequence>`__;
- an ``unsigned long long`` value ``z``;
- an output stream ``output_stream``;
- an input stream ``input_stream``;
- an array type ``Array`` of underlying type ``StateType`` and of size determined by ``Generator::state_size``;
- a `lvalue <https://en.cppreference.com/w/c/language/value_category#Lvalue_expressions>`__ ``lgenerator`` of type ``Generator``;

the following expressions are valid and have their specified effects. 

+----------------------------------+----------------------------------+------------------------------------------------------------------------------------------+
| Expression                       | Return type                      | Requirements                                                                             |
+==================================+==================================+==========================================================================================+
| ``Generator::result_type``       | ``ResultType``                   | ``ResultType`` is an integral type which represents the type of the generated integers.  |
+----------------------------------+----------------------------------+------------------------------------------------------------------------------------------+
| ``Generator::state_type``        | ``StateType``                    | ``StateType`` is an integral type which represents the type of the integer(s)            |
|                                  |                                  | representing the generator's internal state.                                             |
+----------------------------------+----------------------------------+------------------------------------------------------------------------------------------+
| ``Generator::state_size``        | ``size_t``                       | Number of integers of type ``StateType`` in the generator's internal state.              |
+----------------------------------+----------------------------------+------------------------------------------------------------------------------------------+
| ``Generator::default_seed``      | ``StateType``                    | Default seed used to default-construct the generator.                                    |
+----------------------------------+----------------------------------+------------------------------------------------------------------------------------------+
| ``Generator::min()``             | ``ResultType``                   | An inclusive lower bound on the values taken by the generator. This member function is   |
|                                  |                                  | ``constexpr``.                                                                           |
+----------------------------------+----------------------------------+------------------------------------------------------------------------------------------+
| ``Generator::max()``             | ``ResultType``                   | An inclusive upper bound on the values taken by the generator. The value is strictly     |
|                                  |                                  | greater than ``Generator::min()``. This member function is ``constexpr``.                |
+----------------------------------+----------------------------------+------------------------------------------------------------------------------------------+
| ``generator.operator()()``       | ``ResultType``                   | A sample of the generator in the closed interval                                         |
|                                  |                                  | ``[Generator::min(), Generator::max()]``.                                                |
+----------------------------------+----------------------------------+------------------------------------------------------------------------------------------+
| ``Generator()``                  | N/A                              | Constructs a generator with the same state as all other default-constructed engines      |
|                                  |                                  | of type ``Generator``.                                                                   |
+----------------------------------+----------------------------------+------------------------------------------------------------------------------------------+
| ``Generator(other_generator)``   |  N/A                             | Constructs a generator with the same internal state as ``other_generator``.              |
+----------------------------------+----------------------------------+------------------------------------------------------------------------------------------+
| ``Generator(seed)``              |  N/A                             | Constructs a generator whose initial internal state is determined by the integer         |
|                                  |                                  | ``seed``.                                                                                |
+----------------------------------+----------------------------------+------------------------------------------------------------------------------------------+
| ``Generator(sequence)``          |  N/A                             | Constructs a generator whose initial internal state is determined by a single call       |
|                                  |                                  | to ``sequence.generate``.                                                                |
+----------------------------------+----------------------------------+------------------------------------------------------------------------------------------+
| ``generator.seed()``             | ``void``                         | Sets ``generator == Generator()``.                                                       |
+----------------------------------+----------------------------------+------------------------------------------------------------------------------------------+
| ``generator.seed(seed)``         | ``void``                         | Sets ``generator == Generator(seed)``.                                                   |
+----------------------------------+----------------------------------+------------------------------------------------------------------------------------------+
| ``generator.seed(sequence)``     | ``void``                         | Sets ``generator == Generator(sequence)``.                                               |
+----------------------------------+----------------------------------+------------------------------------------------------------------------------------------+
| ``generator.discard(z)``         | ``void``                         | Advances ``generator``'s state as if by ``z`` consecutive calls to ``generator()``.      |
+----------------------------------+----------------------------------+------------------------------------------------------------------------------------------+
| ``generator == other_generator`` | ``bool``                         | True if and only if ``generator`` and ``other_generator`` are in the same internal state |
|                                  |                                  | (such that repeated future calls to ``generator()`` and ``other_generator()`` will       |
|                                  |                                  | produce identical sequences).                                                            |
+----------------------------------+----------------------------------+------------------------------------------------------------------------------------------+
| ``generator != other_generator`` | ``bool``                         | Equal to ``!(generator == other_generator)``.                                            |
+----------------------------------+----------------------------------+------------------------------------------------------------------------------------------+
| ``output_stream << generator``   | ``decltype(output_stream)&``     | Writes to ``output_stream`` the textual representation of ``generator``'s current        |
|                                  |                                  | internal state. In the output, adjacent numbers are separated by one or more space       |
|                                  |                                  | characters. The stream's ``fmtflag`` and fill character are unchanged.                   |
+----------------------------------+----------------------------------+------------------------------------------------------------------------------------------+
| ``input_stream >> lgenerator``   | ``decltype(input_stream)&``      | Reads from ``input_stream`` the textual representation of ``lgenerator``'s current       |
|                                  |                                  | internal state, such that if that state was previously written via                       |
|                                  |                                  | ``output_stream << generator``, then ``generator == lgenerator``. The stream's           |
|                                  |                                  | ``fmtflag`` and fill character are unchanged.                                            |
+----------------------------------+----------------------------------+------------------------------------------------------------------------------------------+





