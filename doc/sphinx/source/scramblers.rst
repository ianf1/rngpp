.. _scramblers:

Scramblers
==========

The library uses a number of so-called scramblers to *scramble* the output of an RNG.
The following scramblers are provided.

Coordinate
----------

*#include <rngpp/scramblers/coordinate.hpp>*

.. doxygenstruct:: rngpp::scramblers::coordinate
   :members:

Multiplication
--------------

*#include <rngpp/scramblers/multiplication.hpp>*

.. doxygenstruct:: rngpp::scramblers::multiplication
   :members:

Plus
----

*#include <rngpp/scramblers/plus.hpp>*

.. doxygenstruct:: rngpp::scramblers::plus
   :members:

Right shift
-----------

*#include <rngpp/scramblers/right_shift.hpp>*

.. doxygenstruct:: rngpp::scramblers::right_shift
   :members:

Starstar
--------

*#include <rngpp/scramblers/starstar.hpp>*

.. doxygenstruct:: rngpp::scramblers::starstar
   :members:

Type conversion
---------------

*#include <rngpp/scramblers/type_conversion.hpp>*

.. doxygenstruct:: rngpp::scramblers::type_conversion
   :members: