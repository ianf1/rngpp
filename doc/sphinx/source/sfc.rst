.. _sfc_reference:

Chris Doty-Humphrey's Small Fast Counting RNG
=============================================

*#include <rngpp/sfc.hpp>*

Engine
------

.. doxygenclass:: rngpp::sfc_engine
   :members:

Convenience typedefs
--------------------

.. doxygentypedef:: rngpp::sfc32

.. doxygentypedef:: rngpp::sfc64
