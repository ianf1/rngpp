.. _reference:

Reference
=========

You will find here the detailed reference of the public interface of the RNG++ library.

.. toctree::
   :maxdepth: 2

   jsf.rst
   mulberry32.rst
   sfc.rst
   splitmix64.rst
   xorshift.rst
   xoshiro.rst
