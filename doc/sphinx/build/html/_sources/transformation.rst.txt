.. _transformation_reference:

Transformation generator
========================

*#include <rngpp/transformation.hpp>*

Engine
------

.. doxygenclass:: rngpp::transformation_engine
   :members:
