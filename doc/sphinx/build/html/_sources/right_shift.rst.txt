.. _right_shift_reference:

Right shift generator
=====================

*#include <rngpp/right_shift.hpp>*

Scrambler
---------

.. doxygenstruct:: rngpp::right_shift_scrambler
   :members:

.. doxygenstruct:: rngpp::change_result_type_scrambler
   :members:

Convenience typedefs
--------------------

.. doxygentypedef:: rngpp::right_shift_engine
