RNG++ documentation
===================

RNG++ is a header-only C++17 library which provides a collection of `random number generators (RNGs) <https://en.wikipedia.org/wiki/Pseudorandom_number_generator>`__ which are faster and/or exhibit better statistical properties than those in the C++ standard library (notably `mt19937 <https://en.cppreference.com/w/cpp/numeric/random/mersenne_twister_engine>`__, `mt19937_64 <https://en.cppreference.com/w/cpp/numeric/random/mersenne_twister_engine>`__, `ranlux24 <https://en.cppreference.com/w/cpp/numeric/random>`__ and `ranlux48 <https://en.cppreference.com/w/cpp/numeric/random>`__).
The generators in RNG++ are intended as drop-in replacements for the ones in the C++ standard library, so as to minimize the hassle of switching over to the library.
Thus, all the provided RNGs satisfy `RandomNumberEngine <https://en.cppreference.com/w/cpp/named_req/RandomNumberEngine>`__.

As a motivating example, RNG++ provides generators which are faster (see :ref:`benchmarks`), pass more stringent statistical tests (see :ref:`rng_quality`) and have a smaller memory footprint, compared to the widely used `mt19937 <https://en.cppreference.com/w/cpp/numeric/random/mersenne_twister_engine>`__ generator.

The main application that the library is geared towards is the fast generation of random numbers for use in `Monte Carlo simulation <https://en.wikipedia.org/wiki/Monte_Carlo_method>`__.
As such, most of the provided generators are not `cryptographically secure <https://en.wikipedia.org/wiki/Cryptographically_secure_pseudorandom_number_generator>`__.

There are no external dependencies, as one of the aims is to make the library as easy to install and use as possible.
In addition, the library depends only on a few small C++ standard library headers.

.. toctree::
   :maxdepth: 2
   
   preface.rst
   credits.rst
   examples.rst
   benchmarks.rst
   rng_quality.rst
   validation_values.rst
   interface.rst
   scramblers.rst
   reference.rst
