//! @file
// Copyright Ian Flint 2019, distributed under the MIT License.

#ifndef RNGPP_INCLUDE_ARRAY_STATE_POLICY
#define RNGPP_INCLUDE_ARRAY_STATE_POLICY

#include <rngpp/utility/array_helpers.hpp>
#include <rngpp/utility/low_bits_mask.hpp>
#include <rngpp/utility/save_stream_flags.hpp>

#include <cstddef>
#include <cstdint>
#include <iosfwd>
#include <limits>
#include <type_traits>

namespace rngpp {
namespace detail_state_policy {

template <typename Array>
class array_state_policy {
 protected:
  Array state_;

 public:
  using array_type = Array;
  using state_type = utility::value_type_t<array_type>;
  static constexpr std::size_t state_size = utility::size_v<array_type>;

  template <typename... Args,
            typename std::enable_if_t<
                sizeof...(Args) == state_size &&
                std::conjunction_v<std::is_convertible<Args, state_type>...>>* =
                nullptr>
  explicit array_state_policy(Args... args) noexcept : state_{args...} {}
  array_state_policy() noexcept : state_{} {}

  template <typename CharT, typename Traits>
  std::basic_ostream<CharT, Traits>& io(
      std::basic_ostream<CharT, Traits>& output_stream) const {
    utility::save_stream_flags s{output_stream};

    const auto space{output_stream.widen(' ')};
    using basic_ostream = std::basic_ostream<CharT, Traits>;
    output_stream.flags(basic_ostream::dec | basic_ostream::fixed |
                        basic_ostream::left);
    output_stream.fill(space);

    std::size_t index{0};
    output_stream << state_[index];

    while (++index != state_size) {
      output_stream << space << state_[index];
    }

    return output_stream;
  }

  template <typename CharT, typename Traits>
  std::basic_istream<CharT, Traits>& io(
      std::basic_istream<CharT, Traits>& input_stream) {
    utility::save_stream_flags s{input_stream};

    using basic_istream = std::basic_istream<CharT, Traits>;
    input_stream.flags(basic_istream::dec | basic_istream::skipws);

    for (auto& x : state_) {
      input_stream >> x;
    }

    return input_stream;
  }

  template <typename OtherArray>
  void set_state(const OtherArray& other) {
    static_assert(utility::size_v<OtherArray> == state_size,
                  "Input array does not have the right size.");
    for (std::size_t i{0}; i < state_size; ++i) {
      state_[i] = other[i];
    }
  }

  template <typename OtherArray>
  [[nodiscard]] bool is_state_equal_to(const OtherArray& other) const {
    if constexpr (utility::size_v<OtherArray> != state_size) {
      return false;
    }
    for (std::size_t i{0}; i < state_size; ++i) {
      if (state_[i] != other[i]) {
        return false;
      }
    }
    return true;
  }

  template <unsigned int WordSize = std::numeric_limits<state_type>::digits,
            typename Sseq>
  void seed(Sseq& sequence) {
    constexpr unsigned int digits{std::numeric_limits<state_type>::digits};
    static_assert(WordSize <= digits);
    if constexpr (WordSize <= 32U) {
      sequence.generate(state_, state_ + state_size);
      if constexpr (WordSize < 32U && WordSize < digits) {
        for (auto& s : state_) {
          s &= utility::low_bits_mask_v<WordSize, state_type>;
        }
      }
    } else {
      constexpr unsigned int k{(WordSize + 31U) / 32U};
      static_assert(k >= 1);
      static_assert(k == 1 || WordSize > 32U);
      constexpr std::size_t seed_size{state_size * k};
      std::uint_least32_t seeds32[seed_size];
      std::size_t seeds32_iterator{0};
      sequence.generate(seeds32, seeds32 + seed_size);

      for (auto& s : state_) {
        state_type sum{0};
        state_type factor{1};
        for (unsigned int i{0}; i < k; ++i) {
          sum += seeds32[seeds32_iterator++] * factor;
          static_assert(32 < digits, "True by definition.");
          factor *= state_type(1) << 32;
        }
        if constexpr (WordSize < digits) {
          s = sum & utility::low_bits_mask_v<WordSize, state_type>;
        } else {
          s = sum;
        }
      }
    }
  }
};

template <typename StateType, std::size_t StateSize>
using c_style_array_state_policy = array_state_policy<StateType[StateSize]>;

}  // namespace detail_state_policy
}  // namespace rngpp

#endif  // RNGPP_INCLUDE_ARRAY_STATE_POLICY
