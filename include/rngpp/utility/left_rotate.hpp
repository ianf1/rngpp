//! @file
// Copyright Ian Flint 2019, distributed under the MIT License.

#ifndef RNGPP_INCLUDE_LEFT_ROTATE
#define RNGPP_INCLUDE_LEFT_ROTATE

#include <limits>

namespace rngpp {
namespace utility {

//! brief Implements the [circular
//! shift](https://en.wikipedia.org/wiki/Circular_shift) operation.
template <int W, typename UnsignedInt>
[[nodiscard]] constexpr inline UnsignedInt rotl(UnsignedInt x, int k) {
  static_assert(W > 0 && ((W - 1) & W) == 0,
                "W should be a positive power of two, typically 32 or 64.");
  constexpr auto mask = static_cast<int>(W - 1);
  k &= mask;
  return (x << k) | (x >> ((-k) & mask));
}

template <typename UnsignedInt>
[[nodiscard]] constexpr inline UnsignedInt rotl(UnsignedInt x, int k) {
  return rotl<std::numeric_limits<UnsignedInt>::digits>(x, k);
}

}  // namespace utility
}  // namespace rngpp

#endif  // RNGPP_INCLUDE_LEFT_ROTATE
