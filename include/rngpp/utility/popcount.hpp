//! @file
// Copyright Ian Flint 2019, distributed under the MIT License.

#ifndef RNGPP_INCLUDE_POPCOUNT
#define RNGPP_INCLUDE_POPCOUNT

#include <type_traits>

#ifdef _MSC_VER
#include <intrin.h>
#include <cstdint>
#endif

namespace rngpp {
namespace utility {

template <typename IntegralType>
[[nodiscard]] constexpr inline auto popcount(IntegralType n) noexcept {
  if constexpr (std::is_integral_v<IntegralType> &&
                !std::is_same_v<IntegralType, bool>) {
#ifdef __GNUC__
    if constexpr (std::is_same_v<std::make_unsigned_t<IntegralType>,
                                 unsigned long int>) {
      return __builtin_popcountl(static_cast<unsigned long int>(n));
    } else if constexpr (std::is_same_v<std::make_unsigned_t<IntegralType>,
                                        unsigned long long int>) {
      return __builtin_popcountll(static_cast<unsigned long long int>(n));
    } else {
      return __builtin_popcount(static_cast<unsigned int>(n));
    }
#elif defined(_MSC_VER)
    if constexpr (std::is_same_v<std::make_unsigned_t<IntegralType>,
                                 unsigned short>) {
      return __popcnt16(static_cast<unsigned short>(n));
    } else if constexpr (std::is_same_v<std::make_unsigned_t<IntegralType>,
                                        std::uint64_t>) {
      return __popcnt64(static_cast<std::uint64_t>(n));
    } else {
      return __popcnt(static_cast<unsigned int>(n));
    }
#else
    static_assert(!std::is_same_v<IntegralType, IntegralType>,
                  "Function not implemented on this compiler.");
#endif
  } else {
    static_assert(!std::is_same_v<IntegralType, IntegralType>,
                  "Function not implemented for non-integral types.");
  }
}

}  // namespace utility
}  // namespace rngpp

#endif  // RNGPP_INCLUDE_POPCOUNT
