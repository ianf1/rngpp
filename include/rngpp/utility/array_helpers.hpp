//! @file
// Copyright Ian Flint 2019, distributed under the MIT License.

#ifndef RNGPP_INCLUDE_ARRAY
#define RNGPP_INCLUDE_ARRAY

#include <cstddef>

namespace rngpp {
namespace utility {
namespace detail_array {
template <typename ArrayType>
[[nodiscard]] inline constexpr auto size(const ArrayType& array) {
  return array.size();
}

template <typename T, std::size_t N>
[[nodiscard]] inline constexpr auto size([[maybe_unused]] const T (&array)[N]) noexcept {
  return N;
}

template <typename ArrayType>
struct value_type_implementation {
  using type = typename ArrayType::value_type;
};

template <typename T, std::size_t N>
struct value_type_implementation<T[N]> {
  using type = T;
};

}  // namespace detail_array

template <typename ArrayType>
constexpr auto size_v = detail_array::size(ArrayType{});

template <typename ArrayType>
using value_type_t = typename detail_array::value_type_implementation<ArrayType>::type;

}  // namespace utility
}  // namespace rngpp

#endif  // RNGPP_INCLUDE_ARRAY
