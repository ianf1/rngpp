//! @file
// Copyright Ian Flint 2019, distributed under the MIT License.

#ifndef RNGPP_INCLUDE_IS_SEED_SEQ
#define RNGPP_INCLUDE_IS_SEED_SEQ

#include <type_traits>

namespace rngpp {
namespace utility {
namespace detail_type_traits {

//! @brief Hand-written ``std::experimental::nonesuch``, see
//! https://en.cppreference.com/w/cpp/experimental/nonesuch.
struct nonesuch {
  ~nonesuch() = delete;
  nonesuch(nonesuch const&) = delete;
  void operator=(nonesuch const&) = delete;
};

template <typename Default, typename AlwaysVoid,
          template <typename...> typename Op, typename... Args>
struct detector {
  using value_t = std::false_type;
  using type = Default;
};

template <typename Default, template <typename...> typename Op,
          typename... Args>
struct detector<Default, std::void_t<Op<Args...>>, Op, Args...> {
  using value_t = std::true_type;
  using type = Op<Args...>;
};

template <template <typename...> typename Op, typename... Args>
using detected_t = typename detector<nonesuch, void, Op, Args...>::type;

//! @brief Hand-written ``std::experimental::is_detected_exact``, see
//! https://en.cppreference.com/w/cpp/experimental/is_detected/
template <typename Expected, template <typename...> typename Op,
          typename... Args>
using is_detected_exact = std::is_same<Expected, detected_t<Op, Args...>>;

// The technique below which requires default constructors is to avoid use of
// std::declval and the associated dependency on <utility>
using ArrayTestType = int[1];
template <typename T>
using has_generate_t =
    decltype(T{}.generate(ArrayTestType{}[0], ArrayTestType{}[0]));

template <typename T>
using has_generate = is_detected_exact<void, has_generate_t, T>;

}  // namespace detail_type_traits

template <typename T, typename Generator>
using is_seed_seq = std::conjunction<
    detail_type_traits::has_generate<T>,
    std::negation<std::is_convertible<std::remove_cv_t<T>, Generator>>>;

template <typename T, typename Generator>
constexpr bool is_seed_seq_v = is_seed_seq<T, Generator>::value;

}  // namespace utility
}  // namespace rngpp

#endif  // RNGPP_INCLUDE_IS_SEED_SEQ
