//! @file
// Copyright Ian Flint 2019, distributed under the MIT License.

#ifndef RNGPP_INCLUDE_SAVE_STREAM_FLAGS
#define RNGPP_INCLUDE_SAVE_STREAM_FLAGS

#include <type_traits>

namespace rngpp {
namespace utility {

template <typename Stream>
class save_stream_flags {
  static_assert(
      std::is_same_v<Stream, std::remove_cv_t<std::remove_reference_t<Stream>>>,
      "The class should not be used with reference/const/volatile types.");

 private:
  Stream& stream_;
  const typename Stream::fmtflags flags_;
  const typename Stream::char_type fill_;

 public:
  save_stream_flags(const save_stream_flags&) = delete;
  save_stream_flags& operator=(const save_stream_flags&) = delete;
  explicit save_stream_flags(Stream& stream)
      : stream_{stream}, flags_{stream.flags()}, fill_{stream.fill()} {}
  ~save_stream_flags() {
    stream_.flags(flags_);
    stream_.fill(fill_);
  }
};

template <typename Stream>
save_stream_flags(Stream&)->save_stream_flags<std::remove_cv_t<Stream>>;

}  // namespace utility
}  // namespace rngpp

#endif  // RNGPP_INCLUDE_SAVE_STREAM_FLAGS
