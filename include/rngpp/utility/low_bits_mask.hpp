//! @file
// Copyright Ian Flint 2019, distributed under the MIT License.

#ifndef RNGPP_INCLUDE_LOW_BITS_MASK
#define RNGPP_INCLUDE_LOW_BITS_MASK

#include <cstddef>
#include <limits>

namespace rngpp {
namespace utility {
namespace detail_low_bits_mask {

template <unsigned int Bits, typename UnsignedInt>
constexpr inline UnsignedInt get_low_bits_mask() noexcept {
  constexpr unsigned int digits{std::numeric_limits<UnsignedInt>::digits};
  static_assert(Bits <= digits, "Invalid Bits parameter.");
  if constexpr (Bits == digits) {  // Avoid overflow in this case.
    return static_cast<UnsignedInt>(~(UnsignedInt(0)));
  } else {
    return static_cast<UnsignedInt>(
        ~(static_cast<UnsignedInt>(~(UnsignedInt(0))) << Bits));
  }
}

template <unsigned int Bits, typename UnsignedInt>
struct low_bits_mask {
  static constexpr UnsignedInt value = get_low_bits_mask<Bits, UnsignedInt>();
};

}  // namespace detail_low_bits_mask

//! @brief Returns a value \f$2^{Bits}-1\f$ of type ``UnsignedInt``.
template <unsigned int Bits, typename UnsignedInt = std::size_t>
constexpr auto low_bits_mask_v =
    detail_low_bits_mask::low_bits_mask<Bits, UnsignedInt>::value;

}  // namespace utility
}  // namespace rngpp

#endif  // RNGPP_INCLUDE_LOW_BITS_MASK
