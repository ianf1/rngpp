//! @file
// Copyright Ian Flint 2019, distributed under the MIT License.

#ifndef RNGPP_INCLUDE_SCRAMBLERS_RIGHT_SHIFT
#define RNGPP_INCLUDE_SCRAMBLERS_RIGHT_SHIFT

#include <rngpp/scramblers/coordinate.hpp>

#include <limits>

namespace rngpp {
namespace scramblers {

//! @brief A scrambler which right-shifts the output of another scrambler.
//!
//! @tparam Shift Shift.
//! @tparam Scrambler Another scrambler.
template <unsigned int Shift, typename Scrambler = scramblers::coordinate<0>>
struct right_shift {
  template <typename T>
  [[nodiscard]] static constexpr auto min(T lower_input_range,
                                          T upper_input_range) {
    const auto m{Scrambler::min(lower_input_range, upper_input_range)};
    return m >> Shift;
  }

  template <typename T>
  [[nodiscard]] static constexpr auto max(T lower_input_range,
                                          T upper_input_range) {
    const auto M{Scrambler::max(lower_input_range, upper_input_range)};
    return M >> Shift;
  }

  template <typename ArrayType>
  [[nodiscard]] static constexpr auto apply(const ArrayType& state) {
    const auto applied{Scrambler::apply(state)};
    static_assert(Shift < std::numeric_limits<decltype(applied)>::digits,
                  "Invalid shift value.");
    return applied >> Shift;
  }
};

}  // namespace scramblers
}  // namespace rngpp

#endif  // RNGPP_INCLUDE_SCRAMBLERS_RIGHT_SHIFT
