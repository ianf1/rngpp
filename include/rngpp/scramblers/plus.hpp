//! @file
// Copyright Ian Flint 2019, distributed under the MIT License.

#ifndef RNGPP_INCLUDE_SCRAMBLERS_PLUS
#define RNGPP_INCLUDE_SCRAMBLERS_PLUS

#include <limits>

namespace rngpp {
namespace scramblers {

//! @brief A scrambler which sums the outputs of two given scramblers.
//!
//! @tparam Scrambler1 First scrambler.
//! @tparam Scrambler2 Second scrambler.
template <typename Scrambler1, typename Scrambler2>
struct plus {
  template <typename T>
  [[nodiscard]] static constexpr auto min(T lower_input_range,
                                          T upper_input_range) noexcept {
    const auto m{Scrambler1::min(lower_input_range, upper_input_range) +
                 Scrambler2::min(lower_input_range, upper_input_range)};
    using ComputationType = decltype(m);
    return std::numeric_limits<ComputationType>::min();
  }

  template <typename T>
  [[nodiscard]] static constexpr auto max(T lower_input_range,
                                          T upper_input_range) noexcept {
    const auto M{Scrambler1::max(lower_input_range, upper_input_range) +
                 Scrambler2::max(lower_input_range, upper_input_range)};
    using ComputationType = decltype(M);
    return std::numeric_limits<ComputationType>::max();
  }

  template <typename ArrayType>
  [[nodiscard]] static constexpr auto apply(const ArrayType& state) {
    return Scrambler1::apply(state) + Scrambler2::apply(state);
  }
};

}  // namespace scramblers
}  // namespace rngpp

#endif  // RNGPP_INCLUDE_SCRAMBLERS_PLUS
