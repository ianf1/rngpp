//! @file
// Copyright Ian Flint 2019, distributed under the MIT License.

#ifndef RNGPP_INCLUDE_SCRAMBLERS_TYPE_CONVERSION
#define RNGPP_INCLUDE_SCRAMBLERS_TYPE_CONVERSION

#include <rngpp/scramblers/coordinate.hpp>

#include <limits>

namespace rngpp {
namespace scramblers {

//! @brief A scrambler which casts the output of another scrambler to another type.
//!
//! @tparam ResultType Type to cast to.
//! @tparam Scrambler Another scrambler.
template <typename ResultType, typename Scrambler = scramblers::coordinate<0>>
struct type_conversion {
  template <typename T>
  [[nodiscard]] static constexpr auto min(T lower_input_range,
                                          T upper_input_range) {
    const auto m{Scrambler::min(lower_input_range, upper_input_range)};
    const auto M{Scrambler::max(lower_input_range, upper_input_range)};

#ifdef __INTEL_COMPILER
#pragma warning push
#pragma warning disable 186
#endif

    if (m < std::numeric_limits<ResultType>::min() ||
        M > std::numeric_limits<ResultType>::max()) {
      return std::numeric_limits<ResultType>::min();
    }

#ifdef __INTEL_COMPILER
#pragma warning pop
#endif

    return ResultType(m);
  }

  template <typename T>
  [[nodiscard]] static constexpr auto max(T lower_input_range,
                                          T upper_input_range) {
    const auto m{Scrambler::min(lower_input_range, upper_input_range)};
    const auto M{Scrambler::max(lower_input_range, upper_input_range)};

#ifdef __INTEL_COMPILER
#pragma warning push
#pragma warning disable 186
#endif

    if (m < std::numeric_limits<ResultType>::min() ||
        M > std::numeric_limits<ResultType>::max()) {
      return std::numeric_limits<ResultType>::max();
    }

#ifdef __INTEL_COMPILER
#pragma warning pop
#endif

    return ResultType(M);
  }

  template <typename ArrayType>
  [[nodiscard]] static constexpr auto apply(const ArrayType& state) {
    return static_cast<ResultType>(Scrambler::apply(state));
  }
};

}  // namespace scramblers
}  // namespace rngpp

#endif  // RNGPP_INCLUDE_SCRAMBLERS_TYPE_CONVERSION
