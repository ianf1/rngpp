//! @file
// Copyright Ian Flint 2019, distributed under the MIT License.

#ifndef RNGPP_INCLUDE_SCRAMBLERS_COORDINATE
#define RNGPP_INCLUDE_SCRAMBLERS_COORDINATE

#include <rngpp/utility/array_helpers.hpp>

#include <cstddef>

namespace rngpp {
namespace scramblers {

//! @brief A scrambler which returns the state at a given index.
//!
//! @tparam Index Index.
template <std::size_t Index = 0>
struct coordinate {
  template <typename T>
  [[nodiscard]] static constexpr auto min(T lower_input_range, T) noexcept {
    return lower_input_range;
  }

  template <typename T>
  [[nodiscard]] static constexpr auto max(T, T upper_input_range) noexcept {
    return upper_input_range;
  }

  template <typename ArrayType>
  [[nodiscard]] static constexpr auto apply(const ArrayType& state) noexcept {
    static_assert(Index < utility::size_v<ArrayType>, "Invalid index.");
    return state[Index];
  }
};

}  // namespace scramblers
}  // namespace rngpp

#endif  // RNGPP_INCLUDE_SCRAMBLERS_COORDINATE
