//! @file
// Copyright Ian Flint 2019, distributed under the MIT License.

#ifndef RNGPP_INCLUDE_SCRAMBLERS_STARSTAR
#define RNGPP_INCLUDE_SCRAMBLERS_STARSTAR

#include <rngpp/scramblers/coordinate.hpp>
#include <rngpp/utility/left_rotate.hpp>

#include <limits>

namespace rngpp {
namespace scramblers {

//! @brief A scrambler which performs a
//! [starstar](http://vigna.di.unimi.it/ftp/papers/ScrambledLinear.pdf)
//! operation on the output of another scrambler.
//!
//! A starstar operation on ``x`` is defined as ``P * rotl(x * Q, R)``, where
//! ``rotl`` denotes the [circular
//! shift](https://en.wikipedia.org/wiki/Circular_shift).
//!
//! @tparam P P as described above.
//! @tparam Q Q as described above.
//! @tparam R R as described above.
//! @tparam Scrambler Another scrambler.
template <unsigned int P, unsigned int Q, unsigned int R,
          typename Scrambler = scramblers::coordinate<0>>
struct starstar {
  template <typename T>
  [[nodiscard]] static constexpr auto min(T lower_input_range,
                                          T upper_input_range) noexcept {
    const auto m{Scrambler::min(lower_input_range, upper_input_range)};
    using ComputationType = decltype(m);
    return std::numeric_limits<ComputationType>::min();
  }

  template <typename T>
  [[nodiscard]] static constexpr auto max(T lower_input_range,
                                          T upper_input_range) noexcept {
    const auto M{Scrambler::max(lower_input_range, upper_input_range)};
    using ComputationType = decltype(M);
    return std::numeric_limits<ComputationType>::max();
  }

  template <typename ArrayType>
  [[nodiscard]] static constexpr auto apply(const ArrayType& state) {
    const auto applied{Scrambler::apply(state)};
    using UnderlyingType = decltype(applied);
    return static_cast<UnderlyingType>(P) *
           utility::rotl(applied * static_cast<UnderlyingType>(Q),
                         static_cast<UnderlyingType>(R));
  }
};

}  // namespace scramblers
}  // namespace rngpp

#endif  // RNGPP_INCLUDE_SCRAMBLERS_STARSTAR
