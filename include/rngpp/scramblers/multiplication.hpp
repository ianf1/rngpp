//! @file
// Copyright Ian Flint 2019, distributed under the MIT License.

#ifndef RNGPP_INCLUDE_SCRAMBLERS_MULTIPLICATION
#define RNGPP_INCLUDE_SCRAMBLERS_MULTIPLICATION

#include <rngpp/scramblers/coordinate.hpp>

#include <limits>

namespace rngpp {
namespace scramblers {

//! @brief A scrambler which multiplies the output of another scrambler by a given ``Multiplier``.
//!
//! @tparam Multiplier Multiplier.
//! @tparam Scrambler Another scrambler.
template <auto Multiplier, typename Scrambler = scramblers::coordinate<0>>
struct multiplication {
  static_assert(Multiplier > 0 &&
                    Multiplier %
                            std::numeric_limits<decltype(Multiplier)>::radix !=
                        0,
                "Multiplication by an even number would not preserve "
                "uniformity in the output range.");

  template <typename T>
  [[nodiscard]] static constexpr auto min(T lower_input_range,
                                          T upper_input_range) noexcept {
    const auto m{Scrambler::min(lower_input_range, upper_input_range)};
    using ComputationType = decltype(m);
    if (m == 1 && std::numeric_limits<ComputationType>::min() ==
                      0) {  // In this case, 0 is unattainable
      return static_cast<ComputationType>(1);
    } else {
      return std::numeric_limits<ComputationType>::min();
    }
  }

  template <typename T>
  [[nodiscard]] static constexpr auto max(T lower_input_range,
                                          T upper_input_range) noexcept {
    const auto M{Scrambler::max(lower_input_range, upper_input_range)};
    using ComputationType = decltype(M);
    return std::numeric_limits<ComputationType>::max();
  }

  template <typename ArrayType>
  [[nodiscard]] static constexpr auto apply(const ArrayType& state) {
    const auto applied{Scrambler::apply(state)};
    return static_cast<decltype(applied)>(Multiplier) * applied;
  }
};

}  // namespace scramblers
}  // namespace rngpp

#endif  // RNGPP_INCLUDE_SCRAMBLERS_MULTIPLICATION
