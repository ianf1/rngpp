./mulberry32_quality | ./RNG_test stdin32 -multithreaded
RNG_test using PractRand version 0.94
RNG = RNG_stdin32, seed = unknown
test set = core, folding = standard (32 bit)

rng=RNG_stdin32, seed=unknown
length= 512 megabytes (2^29 bytes), time= 1.9 seconds
  Test Name                         Raw       Processed     Evaluation
  FPF-14+6/16:all                   R= +10.7  p =  1.7e-9    VERY SUSPICIOUS 
  [Low1/32]BCFN(2+2,13-5,T)         R= +10.3  p =  3.3e-4   unusual          
  ...and 176 test result(s) without anomalies

rng=RNG_stdin32, seed=unknown
length= 1 gigabyte (2^30 bytes), time= 4.0 seconds
  Test Name                         Raw       Processed     Evaluation
  FPF-14+6/16:(0,14-0)              R=  +9.3  p =  2.9e-8   suspicious       
  FPF-14+6/16:(1,14-0)              R= +12.3  p =  5.9e-11   VERY SUSPICIOUS 
  FPF-14+6/16:(2,14-0)              R=  +8.0  p =  4.8e-7   mildly suspicious
  FPF-14+6/16:(3,14-0)              R=  +8.3  p =  2.8e-7   mildly suspicious
  FPF-14+6/16:all                   R= +20.4  p =  1.2e-18    FAIL !         
  ...and 187 test result(s) without anomalies
