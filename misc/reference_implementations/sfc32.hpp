//! @file
#ifndef RNGPP_INCLUDE_REFERENCE_SFC32
#define RNGPP_INCLUDE_REFERENCE_SFC32

#include <cstdint>

namespace rngpp {
namespace detail_sfc32 {

//! From PractRand source code http://pracrand.sourceforge.net/
template <typename Array>
inline std::uint32_t next(Array& s) {
  enum { BARREL_SHIFT = 21, RSHIFT = 9, LSHIFT = 3 };
  std::uint32_t tmp = s[0] + s[1] + s[3]++;
  s[0] = s[1] ^ (s[1] >> RSHIFT);
  s[1] = s[2] + (s[2] << LSHIFT);
  s[2] = ((s[2] << BARREL_SHIFT) | (s[2] >> (32 - BARREL_SHIFT))) + tmp;
  return tmp;
}

template <typename Array>
inline void seed(Array& a, std::uint32_t seed) {
  a[0] = seed;
  a[1] = seed;
  a[2] = seed;
  a[3] = 1;
  for (int i = 0; i < 8; i++) next(a);
}

}  // namespace detail_sfc32
}  // namespace rngpp

#endif  // RNGPP_INCLUDE_REFERENCE_SFC32
