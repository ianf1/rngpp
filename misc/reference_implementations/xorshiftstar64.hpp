//! @file
#ifndef RNGPP_INCLUDE_REFERENCE_XORSHIFTSTAR64
#define RNGPP_INCLUDE_REFERENCE_XORSHIFTSTAR64

#include <cstdint>

namespace rngpp {
namespace detail_xorshiftstar64 {

//! From https://en.wikipedia.org/wiki/Xorshift (with changes)
std::uint64_t next(std::uint64_t& x);
std::uint64_t next(std::uint64_t& x) {
  x ^= x >> 12;
  x ^= x << 25;
  x ^= x >> 27;
  return x * 0x2545F4914F6CDD1DULL;
}

}  // namespace detail_xorshiftstar64
}  // namespace rngpp

#endif  // RNGPP_INCLUDE_REFERENCE_XORSHIFTSTAR64
