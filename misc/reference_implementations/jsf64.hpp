//! @file
#ifndef RNGPP_INCLUDE_REFERENCE_JSF64
#define RNGPP_INCLUDE_REFERENCE_JSF64

#include <cstdint>

namespace rngpp {
namespace detail_jsf64 {

//! From http://burtleburtle.net/bob/rand/smallprng.html
typedef std::uint64_t u4;
typedef struct ranctx {
  u4 a;
  u4 b;
  u4 c;
  u4 d;
} ranctx;

#define rot64(x, k) (((x) << (k)) | ((x) >> (64 - (k))))
u4 next(ranctx& x);
u4 next(ranctx& x) {
  u4 e = x.a - rot64(x.b, 7);
  x.a = x.b ^ rot64(x.c, 13);
  x.b = x.c + rot64(x.d, 37);
  x.c = x.d + e;
  x.d = e + x.a;
  return x.d;
}

void raninit(ranctx& x, u4 seed);
void raninit(ranctx& x, u4 seed) {
  u4 i;
  x.a = 0xf1ea5eed;
  x.b = x.c = x.d = seed;
  for (i = 0; i < 20; ++i) {
    (void)next(x);
  }
}

}  // namespace detail_jsf64
}  // namespace rngpp

#endif  // RNGPP_INCLUDE_REFERENCE_JSF64
