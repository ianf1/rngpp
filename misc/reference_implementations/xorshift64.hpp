//! @file
#ifndef RNGPP_INCLUDE_REFERENCE_XORSHIFT64
#define RNGPP_INCLUDE_REFERENCE_XORSHIFT64

#include <cstdint>

namespace rngpp {
namespace detail_xorshift64 {

//! From https://en.wikipedia.org/wiki/Xorshift (with changes)
std::uint64_t next(std::uint64_t& x);
std::uint64_t next(std::uint64_t& x) {
  x ^= x << 13;
  x ^= x >> 7;
  x ^= x << 17;
  return x;
}

}  // namespace detail_xorshift64
}  // namespace rngpp

#endif  // RNGPP_INCLUDE_REFERENCE_XORSHIFT64
