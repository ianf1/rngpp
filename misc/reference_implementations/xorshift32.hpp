//! @file
#ifndef RNGPP_INCLUDE_REFERENCE_XORSHIFT32
#define RNGPP_INCLUDE_REFERENCE_XORSHIFT32

#include <cstdint>

namespace rngpp {
namespace detail_xorshift32 {

//! From https://en.wikipedia.org/wiki/Xorshift (with changes)
std::uint32_t next(std::uint32_t& x);
std::uint32_t next(std::uint32_t& x) {
  x ^= x << 13;
  x ^= x >> 17;
  x ^= x << 5;
  return x;
}

}  // namespace detail_xorshift32
}  // namespace rngpp

#endif  // RNGPP_INCLUDE_REFERENCE_XORSHIFT32
