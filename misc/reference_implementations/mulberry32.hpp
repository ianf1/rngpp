//! @file
#ifndef RNGPP_INCLUDE_REFERENCE_MULBERRY32
#define RNGPP_INCLUDE_REFERENCE_MULBERRY32

#include <cstdint>

namespace rngpp {
namespace detail_mulberry32 {

//! From https://gist.github.com/tommyettinger/46a874533244883189143505d203312c
std::uint32_t next(std::uint32_t& x);
std::uint32_t next(std::uint32_t& x) {
  std::uint32_t z = (x += std::uint32_t(0x6D2B79F5UL));
  z = (z ^ (z >> 15)) * (z | std::uint32_t(1));
  z ^= z + (z ^ (z >> 7)) * (z | std::uint32_t(61));
  return z ^ (z >> 14);
}

}  // namespace detail_mulberry32
}  // namespace rngpp

#endif  // RNGPP_INCLUDE_REFERENCE_MULBERRY32
