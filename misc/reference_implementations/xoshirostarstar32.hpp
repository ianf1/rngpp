//! @file
#ifndef RNGPP_INCLUDE_REFERENCE_XOSHIROSTARSTAR32
#define RNGPP_INCLUDE_REFERENCE_XOSHIROSTARSTAR32

#include <cstdint>

namespace rngpp {
namespace detail_xoshirostarstar32 {

//! From http://xoshiro.di.unimi.it/xoshiro128starstar.c
inline std::uint32_t rotl32(const std::uint32_t x, int k) {
  return (x << k) | (x >> (32 - k));
}

template <typename Array>
inline std::uint32_t next(Array& s) {
  const std::uint32_t result_starstar = rotl32(s[0] * 5, 7) * 9;
  const std::uint32_t t = s[1] << 9;

  s[2] ^= s[0];
  s[3] ^= s[1];
  s[1] ^= s[2];
  s[0] ^= s[3];
  s[2] ^= t;

  s[3] = rotl32(s[3], 11);

  return result_starstar;
}

template <typename Array>
void jump(Array& s) {
  static const uint32_t JUMP[] = {0x8764000b, 0xf542d2d3, 0x6fa035c3,
                                  0x77f2db5b};

  uint32_t s0 = 0;
  uint32_t s1 = 0;
  uint32_t s2 = 0;
  uint32_t s3 = 0;
  for (unsigned int i = 0; i < sizeof JUMP / sizeof *JUMP; i++)
    for (int b = 0; b < 32; b++) {
      if (JUMP[i] & UINT32_C(1) << b) {
        s0 ^= s[0];
        s1 ^= s[1];
        s2 ^= s[2];
        s3 ^= s[3];
      }
      next(s);
    }

  s[0] = s0;
  s[1] = s1;
  s[2] = s2;
  s[3] = s3;
}

}  // namespace detail_xoshirostarstar32
}  // namespace rngpp

#endif  // RNGPP_INCLUDE_REFERENCE_XOSHIROSTARSTAR32
