//! @file
#ifndef RNGPP_INCLUDE_REFERENCE_XOSHIROPLUS64
#define RNGPP_INCLUDE_REFERENCE_XOSHIROPLUS64

#include <cstdint>

namespace rngpp {
namespace detail_xoshiroplus64 {

//! From http://xoshiro.di.unimi.it/xoshiro256plus.c
inline std::uint64_t rotl64(const std::uint64_t x, int k) {
  return (x << k) | (x >> (64 - k));
}

template <typename Array>
inline std::uint64_t next(Array& s) {
  const std::uint64_t result_plus = s[0] + s[3];
  const std::uint64_t t = s[1] << 17;

  s[2] ^= s[0];
  s[3] ^= s[1];
  s[1] ^= s[2];
  s[0] ^= s[3];
  s[2] ^= t;

  s[3] = rotl64(s[3], 45);

  return result_plus;
}

template <typename Array>
void jump(Array& s) {
  static const uint64_t JUMP[] = {0x180ec6d33cfd0aba, 0xd5a61266f0c9392c,
                                  0xa9582618e03fc9aa, 0x39abdc4529b1661c};

  uint64_t s0 = 0;
  uint64_t s1 = 0;
  uint64_t s2 = 0;
  uint64_t s3 = 0;
  for (unsigned int i = 0; i < sizeof JUMP / sizeof *JUMP; i++)
    for (int b = 0; b < 64; b++) {
      if (JUMP[i] & UINT64_C(1) << b) {
        s0 ^= s[0];
        s1 ^= s[1];
        s2 ^= s[2];
        s3 ^= s[3];
      }
      next(s);
    }

  s[0] = s0;
  s[1] = s1;
  s[2] = s2;
  s[3] = s3;
}

template <typename Array>
void long_jump(Array& s) {
  static const uint64_t LONG_JUMP[] = {0x76e15d3efefdcbbf, 0xc5004e441c522fb3,
                                       0x77710069854ee241, 0x39109bb02acbe635};

  uint64_t s0 = 0;
  uint64_t s1 = 0;
  uint64_t s2 = 0;
  uint64_t s3 = 0;
  for (unsigned int i = 0; i < sizeof LONG_JUMP / sizeof *LONG_JUMP; i++)
    for (int b = 0; b < 64; b++) {
      if (LONG_JUMP[i] & UINT64_C(1) << b) {
        s0 ^= s[0];
        s1 ^= s[1];
        s2 ^= s[2];
        s3 ^= s[3];
      }
      next(s);
    }

  s[0] = s0;
  s[1] = s1;
  s[2] = s2;
  s[3] = s3;
}

}  // namespace detail_xoshiroplus64
}  // namespace rngpp

#endif  // RNGPP_INCLUDE_REFERENCE_XOSHIROPLUS64
