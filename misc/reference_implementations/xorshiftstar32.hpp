//! @file
#ifndef RNGPP_INCLUDE_REFERENCE_XORSHIFTSTAR32
#define RNGPP_INCLUDE_REFERENCE_XORSHIFTSTAR32

#include <cstdint>

namespace rngpp {
namespace detail_xorshiftstar32 {

//! From https://en.wikipedia.org/wiki/Xorshift (with changes)
std::uint32_t next(std::uint64_t& x);
std::uint32_t next(std::uint64_t& x) {
  x ^= x >> 11;
  x ^= x << 31;
  x ^= x >> 18;
  return std::uint32_t((x * 0xd989bcacc137dcd5ULL) >> 32);
}

}  // namespace detail_xorshiftstar32
}  // namespace rngpp

#endif  // RNGPP_INCLUDE_REFERENCE_XORSHIFTSTAR32
