//! @file
#ifndef RNGPP_INCLUDE_REFERENCE_SPLITMIX64
#define RNGPP_INCLUDE_REFERENCE_SPLITMIX64

#include <cstdint>

namespace rngpp {
namespace detail_splitmix64 {

//! From http://xoshiro.di.unimi.it/splitmix64.c
std::uint64_t next(std::uint64_t& x);
std::uint64_t next(std::uint64_t& x) {
  std::uint64_t z = (x += 0x9e3779b97f4a7c15);
  z = (z ^ (z >> 30)) * 0xbf58476d1ce4e5b9;
  z = (z ^ (z >> 27)) * 0x94d049bb133111eb;
  return z ^ (z >> 31);
}

}  // namespace detail_splitmix64
}  // namespace rngpp

#endif  // RNGPP_INCLUDE_REFERENCE_SPLITMIX64
