RNG++
=====

RNG++ is a header-only C++17 library which provides a collection of random number generators which are faster and/or exhibit better statistical properties than those in the C++ standard library.
All the provided RNGs satisfy [RandomNumberEngine](https://en.cppreference.com/w/cpp/named_req/RandomNumberEngine).

Please check the [documentation](https://ianf1.gitlab.io/rngpp/) for details.