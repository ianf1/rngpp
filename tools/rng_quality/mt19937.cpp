#include "print_buffer_to_stdout.hpp"

#include <cstdint>
#include <random>

int main() {
  using Generator = std::mt19937;
  Generator generator{42};

  rngpp::tools::print_buffer_to_stdout<std::uint32_t>(generator);
}
