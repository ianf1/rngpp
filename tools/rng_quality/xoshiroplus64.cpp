#include "print_buffer_to_stdout.hpp"

#include <rngpp/xoshiro.hpp>

#include <cstdint>

int main() {
  using Generator = rngpp::xoshiroplus64;
  Generator generator{42};

  rngpp::tools::print_buffer_to_stdout<std::uint64_t>(generator);
}
