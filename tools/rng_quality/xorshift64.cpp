#include "print_buffer_to_stdout.hpp"

#include <rngpp/xorshift.hpp>

#include <cstdint>

int main() {
  using Generator = rngpp::xorshift64;
  Generator generator{42};

  rngpp::tools::print_buffer_to_stdout<std::uint64_t>(generator);
}
