#include "print_buffer_to_stdout.hpp"

#include <rngpp/splitmix64.hpp>

#include <cstdint>

int main() {
  using Generator = rngpp::splitmix64;
  Generator generator{42};

  rngpp::tools::print_buffer_to_stdout<std::uint64_t>(generator);
}
