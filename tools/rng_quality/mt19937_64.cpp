#include "print_buffer_to_stdout.hpp"

#include <cstdint>
#include <random>

int main() {
  using Generator = std::mt19937_64;
  Generator generator{42};

  rngpp::tools::print_buffer_to_stdout<std::uint64_t>(generator);
}
