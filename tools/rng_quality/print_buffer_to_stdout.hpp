//! @file
#ifndef RNGPP_INCLUDE_PRINT_BUFFER_TO_STDOUT
#define RNGPP_INCLUDE_PRINT_BUFFER_TO_STDOUT

#include <cstddef>
#include <cstdio>

namespace rngpp {
namespace tools {

template <typename Underlying, typename Generator>
inline void print_buffer_to_stdout(Generator& generator) {
  constexpr std::size_t buffer_size{1024 / sizeof(Underlying)};
  Underlying buffer[buffer_size];

  while (true) {
    for (std::size_t i{0}; i < buffer_size; ++i) {
      buffer[i] = static_cast<Underlying>(generator());
    }
    std::fwrite(&buffer, sizeof(buffer[0]), buffer_size, stdout);
  }
}

}  // namespace tools
}  // namespace rngpp

#endif  // RNGPP_INCLUDE_PRINT_BUFFER_TO_STDOUT
