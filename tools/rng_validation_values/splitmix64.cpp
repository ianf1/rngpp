#include <rngpp/splitmix64.hpp>

#include "../../misc/reference_implementations/splitmix64.hpp"

#include <array>
#include <cstddef>
#include <cstdint>
#include <iostream>
#include <random>

int main() {
  auto x{rngpp::splitmix64::default_seed};

  for (std::size_t i{0}; i < 9999; ++i) {
    static_cast<void>(rngpp::detail_splitmix64::next(x));
  }
  std::cout << "Seed validation value: " << rngpp::detail_splitmix64::next(x)
            << std::endl;

  std::seed_seq sequence{};
  std::array<uint_least32_t, 2> seeds{};
  sequence.generate(seeds.begin(), seeds.end());
  x = std::uint64_t(seeds[0]) + (std::uint64_t(seeds[1]) << 32);

  for (std::size_t i{0}; i < 9999; ++i) {
    static_cast<void>(rngpp::detail_splitmix64::next(x));
  }
  std::cout << "std::seed_seq validation value: "
            << rngpp::detail_splitmix64::next(x) << std::endl;
}
