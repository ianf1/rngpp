#include <rngpp/sfc.hpp>

#include "../../misc/reference_implementations/sfc32.hpp"

#include <array>
#include <cstddef>
#include <cstdint>
#include <iostream>
#include <random>

int main() {
  auto x{rngpp::sfc32::default_seed};
  std::array<std::uint32_t, 4> seeds{};
  rngpp::detail_sfc32::seed(seeds, x);

  for (std::size_t i{0}; i < 9999; ++i) {
    static_cast<void>(rngpp::detail_sfc32::next(seeds));
  }
  std::cout << "Seed validation value: " << rngpp::detail_sfc32::next(seeds)
            << std::endl;

  std::seed_seq sequence{};
  sequence.generate(seeds.begin(), seeds.end());

  for (std::size_t i{0}; i < 9999; ++i) {
    static_cast<void>(rngpp::detail_sfc32::next(seeds));
  }
  std::cout << "std::seed_seq validation value: "
            << rngpp::detail_sfc32::next(seeds) << std::endl;
}
