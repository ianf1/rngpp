#include <rngpp/mulberry32.hpp>

#include "../../misc/reference_implementations/mulberry32.hpp"

#include <array>
#include <cstddef>
#include <cstdint>
#include <iostream>
#include <random>

int main() {
  std::uint32_t x{rngpp::mulberry32::default_seed};

  for (std::size_t i{0}; i < 9999; ++i) {
    static_cast<void>(rngpp::detail_mulberry32::next(x));
  }
  std::cout << "Seed validation value: " << rngpp::detail_mulberry32::next(x)
            << std::endl;

  std::seed_seq sequence{};
  std::array<uint_least32_t, 1> seeds{};
  sequence.generate(seeds.begin(), seeds.end());
  x = seeds[0];

  for (std::size_t i{0}; i < 9999; ++i) {
    static_cast<void>(rngpp::detail_mulberry32::next(x));
  }
  std::cout << "std::seed_seq validation value: "
            << rngpp::detail_mulberry32::next(x) << std::endl;
}
