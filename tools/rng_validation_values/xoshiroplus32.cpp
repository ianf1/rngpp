#include <rngpp/mulberry32.hpp>
#include <rngpp/xoshiro.hpp>

#include "../../misc/reference_implementations/xoshiroplus32.hpp"

#include <array>
#include <cstddef>
#include <cstdint>
#include <iostream>
#include <random>

int main() {
  const auto x{rngpp::xoshiroplus32::default_seed};
  rngpp::mulberry32 mb32{x};
  std::array<std::uint_least32_t, 4> seeds{};
  for (auto& s : seeds) {
    s = static_cast<std::uint_least32_t>(mb32());
  }

  for (std::size_t i{0}; i < 9999; ++i) {
    static_cast<void>(rngpp::detail_xoshiroplus32::next(seeds));
  }
  std::cout << "Seed validation value: "
            << rngpp::detail_xoshiroplus32::next(seeds) << std::endl;

  std::seed_seq sequence{};
  sequence.generate(seeds.begin(), seeds.end());

  for (std::size_t i{0}; i < 9999; ++i) {
    static_cast<void>(rngpp::detail_xoshiroplus32::next(seeds));
  }
  std::cout << "std::seed_seq validation value: "
            << rngpp::detail_xoshiroplus32::next(seeds) << std::endl;

  mb32 = rngpp::mulberry32{x};
  for (auto& s : seeds) {
    s = static_cast<std::uint_least32_t>(mb32());
  }
  rngpp::detail_xoshiroplus32::jump(seeds);

  for (std::size_t i{0}; i < 9999; ++i) {
    static_cast<void>(rngpp::detail_xoshiroplus32::next(seeds));
  }
  std::cout << "jump validation value: "
            << rngpp::detail_xoshiroplus32::next(seeds) << std::endl;
}
