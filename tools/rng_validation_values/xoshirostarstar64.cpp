#include <rngpp/splitmix64.hpp>
#include <rngpp/xoshiro.hpp>

#include "../../misc/reference_implementations/xoshirostarstar64.hpp"

#include <array>
#include <cstddef>
#include <cstdint>
#include <iostream>
#include <random>

int main() {
  const auto x{rngpp::xoshirostarstar64::default_seed};
  rngpp::splitmix64 sm64{x};
  std::array<std::uint64_t, 4> seeds{};
  for (auto& s : seeds) {
    s = sm64();
  }

  for (std::size_t i{0}; i < 9999; ++i) {
    static_cast<void>(rngpp::detail_xoshirostarstar64::next(seeds));
  }
  std::cout << "Seed validation value: "
            << rngpp::detail_xoshirostarstar64::next(seeds) << std::endl;

  std::seed_seq sequence{};
  std::array<uint_least32_t, 8> seeds32{};
  sequence.generate(seeds32.begin(), seeds32.end());
  for (std::size_t i{0}; i < 4; ++i) {
    seeds[i] = std::uint64_t(seeds32[2 * i]) +
               (std::uint64_t(seeds32[2 * i + 1]) << 32);
  }

  for (std::size_t i{0}; i < 9999; ++i) {
    static_cast<void>(rngpp::detail_xoshirostarstar64::next(seeds));
  }
  std::cout << "std::seed_seq validation value: "
            << rngpp::detail_xoshirostarstar64::next(seeds) << std::endl;

  sm64 = rngpp::splitmix64{x};
  for (auto& s : seeds) {
    s = sm64();
  }
  rngpp::detail_xoshirostarstar64::jump(seeds);

  for (std::size_t i{0}; i < 9999; ++i) {
    static_cast<void>(rngpp::detail_xoshirostarstar64::next(seeds));
  }
  std::cout << "jump validation value: "
            << rngpp::detail_xoshirostarstar64::next(seeds) << std::endl;

  sm64 = rngpp::splitmix64{x};
  for (auto& s : seeds) {
    s = sm64();
  }
  rngpp::detail_xoshirostarstar64::long_jump(seeds);

  for (std::size_t i{0}; i < 9999; ++i) {
    static_cast<void>(rngpp::detail_xoshirostarstar64::next(seeds));
  }
  std::cout << "long-jump validation value: "
            << rngpp::detail_xoshirostarstar64::next(seeds) << std::endl;
}
