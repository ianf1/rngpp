#include <rngpp/jsf.hpp>

#include "../../misc/reference_implementations/jsf32.hpp"

#include <array>
#include <cstddef>
#include <cstdint>
#include <iostream>
#include <random>

int main() {
  rngpp::detail_jsf32::ranctx x{};
  rngpp::detail_jsf32::raninit(x, rngpp::jsf32::default_seed);

  for (std::size_t i{0}; i < 9999; ++i) {
    static_cast<void>(rngpp::detail_jsf32::next(x));
  }
  std::cout << "Seed validation value: " << rngpp::detail_jsf32::next(x)
            << std::endl;

  std::seed_seq sequence{};
  std::array<uint_least32_t, 4> seeds{};
  sequence.generate(seeds.begin(), seeds.end());
  x = rngpp::detail_jsf32::ranctx{seeds[0], seeds[1], seeds[2], seeds[3]};

  for (std::size_t i{0}; i < 9999; ++i) {
    static_cast<void>(rngpp::detail_jsf32::next(x));
  }
  std::cout << "std::seed_seq validation value: "
            << rngpp::detail_jsf32::next(x) << std::endl;
}
