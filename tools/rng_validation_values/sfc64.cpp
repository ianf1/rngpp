#include <rngpp/sfc.hpp>

#include "../../misc/reference_implementations/sfc64.hpp"

#include <array>
#include <cstddef>
#include <cstdint>
#include <iostream>
#include <random>

int main() {
  auto x{rngpp::sfc64::default_seed};
  std::array<std::uint64_t, 4> seeds{};
  rngpp::detail_sfc64::seed(seeds, x);

  for (std::size_t i{0}; i < 9999; ++i) {
    static_cast<void>(rngpp::detail_sfc64::next(seeds));
  }
  std::cout << "Seed validation value: " << rngpp::detail_sfc64::next(seeds)
            << std::endl;

  std::seed_seq sequence{};
  std::array<uint_least32_t, 8> seeds32{};
  sequence.generate(seeds32.begin(), seeds32.end());
  for (std::size_t i{0}; i < 4; ++i) {
    seeds[i] = std::uint64_t(seeds32[2 * i]) +
               (std::uint64_t(seeds32[2 * i + 1]) << 32);
  }

  for (std::size_t i{0}; i < 9999; ++i) {
    static_cast<void>(rngpp::detail_sfc64::next(seeds));
  }
  std::cout << "std::seed_seq validation value: "
            << rngpp::detail_sfc64::next(seeds) << std::endl;
}
