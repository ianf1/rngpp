#include <rngpp/jsf.hpp>

#include "../../misc/reference_implementations/jsf64.hpp"

#include <array>
#include <cstddef>
#include <cstdint>
#include <iostream>
#include <random>

int main() {
  rngpp::detail_jsf64::ranctx x{};
  rngpp::detail_jsf64::raninit(x, rngpp::jsf64::default_seed);

  for (std::size_t i{0}; i < 9999; ++i) {
    static_cast<void>(rngpp::detail_jsf64::next(x));
  }
  std::cout << "Seed validation value: " << rngpp::detail_jsf64::next(x)
            << std::endl;

  std::seed_seq sequence{};
  std::array<uint_least32_t, 8> seeds{};
  sequence.generate(seeds.begin(), seeds.end());
  x = rngpp::detail_jsf64::ranctx{
      std::uint64_t(seeds[0]) + (std::uint64_t(seeds[1]) << 32),
      std::uint64_t(seeds[2]) + (std::uint64_t(seeds[3]) << 32),
      std::uint64_t(seeds[4]) + (std::uint64_t(seeds[5]) << 32),
      std::uint64_t(seeds[6]) + (std::uint64_t(seeds[7]) << 32)};
  for (std::size_t i{0}; i < 9999; ++i) {
    static_cast<void>(rngpp::detail_jsf64::next(x));
  }
  std::cout << "std::seed_seq validation value: "
            << rngpp::detail_jsf64::next(x) << std::endl;
}
