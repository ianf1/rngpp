// Copyright Ian Flint 2019, distributed under the MIT License.

#include <rngpp/scramblers/starstar.hpp>
#include <rngpp/utility/left_rotate.hpp>

#include <limits>
#include <type_traits>

using TestType = unsigned long int;
constexpr unsigned int P = 5;
constexpr unsigned int Q = 9;
constexpr unsigned int R = 15;
using Scrambler = rngpp::scramblers::starstar<P, Q, R>;
using OtherScrambler =
    rngpp::scramblers::starstar<P, Q, R, rngpp::scramblers::coordinate<1>>;

constexpr TestType first = 1;
constexpr TestType second = 3;
constexpr TestType state[2] = {first, second};

static_assert(Scrambler::apply(state) == P * rngpp::utility::rotl(first * Q, R));
static_assert(OtherScrambler::apply(state) == P * rngpp::utility::rotl(second * Q, R));
static_assert(std::is_same_v<decltype(Scrambler::apply(state)), TestType>);

static_assert(Scrambler::min(TestType(72), TestType(196)) ==
              std::numeric_limits<TestType>::min());
static_assert(std::is_same_v<
              decltype(Scrambler::min(TestType(72), TestType(196))), TestType>);

static_assert(OtherScrambler::max(TestType(72), TestType(196)) ==
              std::numeric_limits<TestType>::max());
static_assert(
    std::is_same_v<decltype(OtherScrambler::max(TestType(72), TestType(196))),
                   TestType>);
