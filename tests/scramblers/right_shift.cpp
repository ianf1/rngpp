// Copyright Ian Flint 2019, distributed under the MIT License.

#include <rngpp/scramblers/right_shift.hpp>

#include <type_traits>

using TestType = long int;
constexpr unsigned int shift = 4;
using Scrambler = rngpp::scramblers::right_shift<shift>;
using OtherScrambler =
    rngpp::scramblers::right_shift<shift, rngpp::scramblers::coordinate<1>>;

constexpr TestType first = 2;
constexpr TestType second = 172;
constexpr TestType state[2] = {first, second};

static_assert(Scrambler::apply(state) == first >> shift);
static_assert(OtherScrambler::apply(state) == second >> shift);
static_assert(std::is_same_v<decltype(Scrambler::apply(state)), TestType>);

static_assert(Scrambler::min(TestType(72), TestType(196)) == 72 >> shift);
static_assert(std::is_same_v<
              decltype(Scrambler::min(TestType(72), TestType(196))), TestType>);

static_assert(OtherScrambler::max(TestType(72), TestType(196)) == 196 >> shift);
static_assert(
    std::is_same_v<decltype(OtherScrambler::max(TestType(72), TestType(196))),
                   TestType>);
