// Copyright Ian Flint 2019, distributed under the MIT License.

#include <rngpp/scramblers/coordinate.hpp>

#include <type_traits>

using TestType = long int;
using Scrambler = rngpp::scramblers::coordinate<0>;
using OtherScrambler = rngpp::scramblers::coordinate<1>;
constexpr TestType first = 1;
constexpr TestType second = 3;
constexpr TestType state[2] = {first, second};

static_assert(Scrambler::apply(state) == first);
static_assert(OtherScrambler::apply(state) == second);
static_assert(std::is_same_v<decltype(Scrambler::apply(state)), TestType>);

static_assert(Scrambler::min(TestType(72), TestType(196)) == 72);
static_assert(std::is_same_v<
              decltype(Scrambler::min(TestType(72), TestType(196))), TestType>);

static_assert(OtherScrambler::max(TestType(72), TestType(196)) == 196);
static_assert(std::is_same_v<
              decltype(OtherScrambler::max(TestType(72), TestType(196))), TestType>);
