// Copyright Ian Flint 2019, distributed under the MIT License.

#include <rngpp/scramblers/multiplication.hpp>

#include <limits>
#include <type_traits>

using TestType = long int;
constexpr std::size_t multiplier = 193785;
using Scrambler = rngpp::scramblers::multiplication<multiplier>;
using OtherScrambler =
    rngpp::scramblers::multiplication<multiplier,
                                      rngpp::scramblers::coordinate<1>>;
constexpr TestType first = 1;
constexpr TestType second = 3;
constexpr TestType state[2] = {first, second};

static_assert(Scrambler::apply(state) == first * multiplier);
static_assert(OtherScrambler::apply(state) == second * multiplier);
static_assert(std::is_same_v<decltype(Scrambler::apply(state)), TestType>);

static_assert(Scrambler::min(TestType(72), TestType(196)) ==
              std::numeric_limits<TestType>::min());
static_assert(std::is_same_v<
              decltype(Scrambler::min(TestType(72), TestType(196))), TestType>);

static_assert(OtherScrambler::max(TestType(72), TestType(196)) ==
              std::numeric_limits<TestType>::max());
static_assert(std::is_same_v<
              decltype(OtherScrambler::max(TestType(72), TestType(196))), TestType>);
