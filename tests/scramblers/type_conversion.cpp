// Copyright Ian Flint 2019, distributed under the MIT License.

#include <rngpp/scramblers/type_conversion.hpp>

#include <cstdint>
#include <type_traits>

using TestType = std::uint32_t;
using ConvertedType = std::uint8_t;
using Scrambler = rngpp::scramblers::type_conversion<ConvertedType>;
using OtherScrambler =
    rngpp::scramblers::type_conversion<ConvertedType,
                                       rngpp::scramblers::coordinate<1>>;
constexpr TestType first = 68962;
constexpr TestType second = 7232001;
constexpr TestType state[2] = {first, second};

static_assert(Scrambler::apply(state) == static_cast<ConvertedType>(first));
static_assert(OtherScrambler::apply(state) ==
              static_cast<ConvertedType>(second));
static_assert(std::is_same_v<decltype(rngpp::scramblers::type_conversion<
                                      ConvertedType>::apply(state)),
                             ConvertedType>);

static_assert(Scrambler::min(TestType(72), TestType(196)) == ConvertedType(72));
static_assert(
    std::is_same_v<decltype(Scrambler::min(TestType(72), TestType(196))),
                   ConvertedType>);

static_assert(OtherScrambler::max(TestType(72), TestType(196)) ==
              ConvertedType(196));
static_assert(
    std::is_same_v<decltype(OtherScrambler::max(TestType(72), TestType(196))),
                   ConvertedType>);
