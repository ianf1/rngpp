// Copyright Ian Flint 2019, distributed under the MIT License.

#include "../external/Catch2/single_include/catch2/catch.hpp"

#include <rngpp/scramblers/coordinate.hpp>
#include <rngpp/scramblers/multiplication.hpp>
#include <rngpp/scramblers/plus.hpp>
#include <rngpp/scramblers/right_shift.hpp>
#include <rngpp/scramblers/starstar.hpp>
#include <rngpp/scramblers/type_conversion.hpp>

#include <algorithm>
#include <array>
#include <cstddef>
#include <cstdint>
#include <limits>
#include <random>

template <typename ArrayType>
void unscoped_info_array(const ArrayType& array) {
  for (const auto& s : array) {
    UNSCOPED_INFO(s);
  }
}

template <typename Scrambler, typename StateType, std::size_t BlockSize,
          std::size_t NumberTests, StateType L, StateType U, auto... Integers,
          typename Generator>
void test_scrambling(Generator& generator) {
  std::uniform_int_distribution<StateType> int_distribution{L, U};
  for (std::size_t i{0}; i < NumberTests; ++i) {
    std::array<StateType, BlockSize> state{};
    std::generate(state.begin(), state.end(),
                  [&generator, &int_distribution]() {
                    return int_distribution(generator);
                  });
    if constexpr (sizeof...(Integers) == 1) {
      using swallow = int[];
      (void)swallow{1, (state[0] = Integers, void(), int{})...};
    }
    const auto min{*std::min_element(state.begin(), state.end())};
    const auto max{*std::max_element(state.begin(), state.end())};
    const auto state_scrambling{Scrambler::apply(state)};
    unscoped_info_array(state);
    REQUIRE(state_scrambling <= Scrambler::max(min, max));
    unscoped_info_array(state);
    REQUIRE(state_scrambling >= Scrambler::min(min, max));
  }
}

TEMPLATE_TEST_CASE("Scramblers.", "[utility][scramblers]",
                   rngpp::scramblers::coordinate<0>,
                   rngpp::scramblers::right_shift<3>,
                   rngpp::scramblers::multiplication<1981414781>,
                   (rngpp::scramblers::plus<rngpp::scramblers::coordinate<0>, rngpp::scramblers::coordinate<1>>),
                   (rngpp::scramblers::starstar<7, 13, 9>),
                   rngpp::scramblers::type_conversion<std::uint16_t>) {
  using Scrambler = TestType;
  using StateType = std::uint32_t;
  using Generator = std::mt19937;

  Generator generator{42};

  constexpr std::size_t block_size{10};

  SECTION("State has 0 as minimal element.") {
    test_scrambling<Scrambler, StateType, block_size, 1, 0,
                    std::numeric_limits<StateType>::max(), 0>(generator);
  }
  SECTION("State has 1 as minimal element.") {
    test_scrambling<Scrambler, StateType, block_size, 1, 0,
                    std::numeric_limits<StateType>::max(), 1>(generator);
  }

  SECTION("No conditions on state values.") {
    test_scrambling<Scrambler, StateType, block_size, 100000,
                    std::numeric_limits<StateType>::min(),
                    std::numeric_limits<StateType>::max()>(generator);
  }
}
