// Copyright Ian Flint 2019, distributed under the MIT License.

#include "../external/Catch2/single_include/catch2/catch.hpp"

#include <rngpp/jsf.hpp>
#include <rngpp/mulberry32.hpp>
#include <rngpp/sfc.hpp>
#include <rngpp/splitmix64.hpp>
#include <rngpp/xorshift.hpp>
#include <rngpp/xoshiro.hpp>

#include <array>
#include <cstddef>
#include <random>
#include <sstream>
#include <type_traits>
#include <utility>

namespace {

template <std::size_t N, typename LHS, typename RHS>
static void require_first_generations_identical(LHS& lhs, RHS& rhs) {
  for (std::size_t i{0}; i < N; ++i) {
    REQUIRE(lhs() == rhs());
  }
}

TEMPLATE_TEST_CASE("Interface of the different RNGs.",
                   "[interface][jsf][mulberry32][sfc][splitmix64][xorshift]["
                   "xorshiftstar][xoshiro]",
                   rngpp::jsf32, rngpp::jsf64, rngpp::mulberry32, rngpp::sfc32,
                   rngpp::sfc64, rngpp::splitmix64, rngpp::xorshift32,
                   rngpp::xorshift64, rngpp::xorshiftstar32,
                   rngpp::xorshiftstar64, rngpp::xoshirostarstar32,
                   rngpp::xoshirostarstar64, rngpp::xoshiroplus32,
                   rngpp::xoshiroplus64) {
  constexpr std::size_t number_tests{1000000};
  constexpr std::size_t test_magic_number{3784};
  using Generator = TestType;
  SECTION("Typedefs and min/max") {
    using result_type = typename Generator::result_type;
    static_assert(std::is_same_v<result_type, result_type>,
                  "Used to remove ``unused'' warning");

    using state_type = typename Generator::state_type;
    static_assert(std::is_same_v<state_type, state_type>,
                  "Used to remove ``unused'' warning");

    constexpr auto default_seed{Generator::default_seed};
    static_assert(default_seed == default_seed,
                  "Used to remove ``unused'' warning");

    constexpr auto state_size{Generator::state_size};
    static_assert(state_size == state_size,
                  "Used to remove ``unused'' warning");

    static_assert(Generator::min() < Generator::max());
  }
  SECTION("Initialization") {
    SECTION("Initialization with identical seeds") {
      Generator generator{test_magic_number};
      Generator other_generator{test_magic_number};
      require_first_generations_identical<number_tests>(generator,
                                                        other_generator);
    }
    SECTION("Default initialization") {
      Generator generator{};
      Generator other_generator{Generator::default_seed};
      require_first_generations_identical<number_tests>(generator,
                                                        other_generator);
    }
    SECTION("Initialization with seed_seq") {
      std::seed_seq sequence{test_magic_number};
      Generator generator{sequence};
      Generator other_generator{sequence};
      require_first_generations_identical<number_tests>(generator,
                                                        other_generator);
    }
    SECTION("Seed with no value") {
      Generator generator{test_magic_number};
      generator.seed();
      Generator other_generator{};
      require_first_generations_identical<number_tests>(generator,
                                                        other_generator);
    }
    SECTION("Seed with value") {
      Generator generator{};
      generator.seed(test_magic_number);
      Generator other_generator{test_magic_number};
      require_first_generations_identical<number_tests>(generator,
                                                        other_generator);
    }
    SECTION("Seed with seed_seq") {
      std::seed_seq sequence{test_magic_number};
      Generator generator{};
      generator.seed(sequence);
      Generator other_generator{sequence};
      require_first_generations_identical<number_tests>(generator,
                                                        other_generator);
    }
  }
  SECTION("Other member functions") {
    Generator generator{test_magic_number};
    Generator other_generator{test_magic_number};
    SECTION("Equal to") {
      REQUIRE(std::as_const(generator) == other_generator);
      static_cast<void>(generator());
      static_cast<void>(other_generator());
      REQUIRE(generator == other_generator);
      generator.discard(number_tests);
      other_generator.discard(number_tests);
      REQUIRE(generator == other_generator);
    }
    SECTION("Not equal to with same type") {
      REQUIRE_FALSE(std::as_const(generator) != other_generator);
      static_cast<void>(generator());
      REQUIRE(generator != other_generator);
      static_cast<void>(other_generator());
      REQUIRE_FALSE(generator != other_generator);
      generator.discard(number_tests);
      REQUIRE(generator != other_generator);
      other_generator.discard(number_tests);
      REQUIRE_FALSE(generator != other_generator);
    }
    SECTION("Serialize") {
      static_cast<void>(generator());
      std::stringstream string_stream{};
      // Add an unusual flag to make sure that it's unchanged during in/out.
      string_stream.flags(std::ios_base::hex);
      const auto saved_flags{string_stream.flags()};
      const auto saved_fill{string_stream.fill()};
      string_stream << generator;
      REQUIRE((string_stream.flags() == saved_flags &&
               string_stream.fill() == saved_fill));
      REQUIRE_FALSE(generator == other_generator);
      string_stream >> other_generator;
      REQUIRE(generator == other_generator);
      string_stream.clear();
      string_stream << other_generator;
      for (std::size_t i{0}; i < 4; ++i) {
        static_cast<void>(generator());
      }
      string_stream >> generator;
      REQUIRE((string_stream.flags() == saved_flags &&
               string_stream.fill() == saved_fill));
      REQUIRE(generator == other_generator);
    }
    SECTION("Discard") {
      for (std::size_t i{0}; i < number_tests; ++i) {
        static_cast<void>(generator());
      }
      other_generator.discard(number_tests);
      REQUIRE(generator == other_generator);
    }
    SECTION("Min/max") {
      for (std::size_t i{0}; i < number_tests; ++i) {
        const auto result{generator()};
#ifdef __INTEL_COMPILER
#pragma warning push
#pragma warning disable 186
#endif
        REQUIRE(result <= Generator::max());
        REQUIRE(result >= Generator::min());
#ifdef __INTEL_COMPILER
#pragma warning pop
#endif
      }
    }
  }
  SECTION("Copy") {
    Generator generator{test_magic_number};
    SECTION("Non-const") {
      const auto other_generator{generator};
      REQUIRE(generator == other_generator);
    }
    SECTION("Const") {
      const auto other_generator{std::as_const(generator)};
      REQUIRE(generator == other_generator);
    }
    SECTION("Rvalue") {
      const auto other_generator{Generator{test_magic_number}};
      REQUIRE(generator == other_generator);
    }
  }
  SECTION("Assignment") {
    SECTION("Non-const") {
      Generator generator{test_magic_number};
      Generator other_generator{};
      other_generator = generator;
      REQUIRE(generator == other_generator);
    }
    SECTION("Const") {
      const Generator generator{test_magic_number};
      Generator other_generator{};
      other_generator = generator;
      REQUIRE(generator == other_generator);
    }
    SECTION("Rvalue") {
      const Generator generator{test_magic_number};
      Generator other_generator{};
      other_generator = Generator{test_magic_number};
      REQUIRE(generator == other_generator);
    }
  }
}

}  // namespace
