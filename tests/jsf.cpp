// Copyright Ian Flint 2019, distributed under the MIT License.

#include "../external/Catch2/single_include/catch2/catch.hpp"

#include <rngpp/jsf.hpp>

#include <array>
#include <cstddef>
#include <cstdint>
#include <random>
#include <sstream>
#include <type_traits>

namespace {

constexpr std::uint32_t seed_validation_value32 = 1800075052UL;
constexpr std::uint32_t seed_seq_validation_value32 = 571867926UL;

constexpr std::uint64_t seed_validation_value64 = 13064610257608532518ULL;
constexpr std::uint64_t seed_seq_validation_value64 = 13908612302685252830ULL;

TEST_CASE("Interface of jsf.", "[interface][jsf]") {
  constexpr std::size_t test_magic_number = 1783;
  SECTION("Equality between different jsf classes") {
    rngpp::jsf32 generator{test_magic_number};
    rngpp::jsf64 other_generator{test_magic_number};
    REQUIRE_FALSE(generator == other_generator);
  }
  SECTION("jsf32") {
    using Generator = rngpp::jsf32;
    static_assert(Generator::min() == 0);
    static_assert(Generator::max() == ~(static_cast<std::uint32_t>(0)));
    static_assert(std::is_same_v<decltype(Generator{}()), std::uint32_t>);
    SECTION(
        "seed_seq (This tests implementation details that are subject to "
        "change)") {
      std::seed_seq sequence{test_magic_number};
      constexpr std::size_t state_size = 4;
      std::array<std::uint_least32_t, state_size> seeds{};
      sequence.generate(seeds.begin(), seeds.end());
      std::stringstream string_stream{};
      string_stream << seeds.at(0);
      for (std::size_t i{1}; i < state_size; ++i) {
        string_stream << ' ' << seeds.at(i);
      }
      Generator generator{};
      string_stream >> generator;

      std::seed_seq same_sequence{test_magic_number};
      Generator other_generator{same_sequence};
      REQUIRE(generator == other_generator);
    }
    SECTION("seed validation value") {
      Generator generator{};
      for (std::size_t i{0}; i < 9999; ++i) {
        static_cast<void>(generator());
      }
      REQUIRE(static_cast<std::uint32_t>(generator()) ==
              seed_validation_value32);
    }
    SECTION("std::seed_seq validation value") {
      std::seed_seq sequence{};
      Generator generator{sequence};
      for (std::size_t i{0}; i < 9999; ++i) {
        static_cast<void>(generator());
      }
      REQUIRE(static_cast<std::uint32_t>(generator()) ==
              seed_seq_validation_value32);
    }
  }
  SECTION("jsf64") {
    using Generator = rngpp::jsf64;
    static_assert(Generator::min() == 0);
    static_assert(Generator::max() == ~(static_cast<std::uint64_t>(0)));
    static_assert(std::is_same_v<decltype(Generator{}()), std::uint64_t>);
    SECTION(
        "seed_seq (This tests implementation details that are subject to "
        "change)") {
      std::seed_seq sequence{test_magic_number};
      constexpr std::size_t state_size = 4;
      std::array<std::uint_least32_t, 2 * state_size> seeds{};
      sequence.generate(seeds.begin(), seeds.end());
      std::stringstream string_stream{};
      string_stream << (static_cast<std::uint64_t>(seeds.at(0)) +
                        (static_cast<std::uint64_t>(seeds.at(1)) << 32));
      for (std::size_t i{1}; i < state_size; ++i) {
        string_stream << ' '
                      << (static_cast<std::uint64_t>(seeds.at(2 * i)) +
                          (static_cast<std::uint64_t>(seeds.at(2 * i + 1))
                           << 32));
      }
      Generator generator{};
      string_stream >> generator;

      std::seed_seq same_sequence{test_magic_number};
      Generator other_generator{same_sequence};
      REQUIRE(generator == other_generator);
    }
    SECTION("seed validation value") {
      Generator generator{};
      for (std::size_t i{0}; i < 9999; ++i) {
        static_cast<void>(generator());
      }
      REQUIRE(generator() == seed_validation_value64);
    }
    SECTION("std::seed_seq validation value") {
      std::seed_seq sequence{};
      Generator generator{sequence};
      for (std::size_t i{0}; i < 9999; ++i) {
        static_cast<void>(generator());
      }
      REQUIRE(generator() == seed_seq_validation_value64);
    }
  }
}

}  // namespace
