// Copyright Ian Flint 2019, distributed under the MIT License.

#include <rngpp/utility/low_bits_mask.hpp>

#include <cstddef>
#include <cstdint>
#include <limits>

static_assert(rngpp::utility::low_bits_mask_v<1, std::uint8_t> ==
              static_cast<std::uint8_t>(1));
static_assert(rngpp::utility::low_bits_mask_v<2, std::uint8_t> ==
              static_cast<std::uint8_t>(3));
static_assert(rngpp::utility::low_bits_mask_v<3, std::uint8_t> ==
              static_cast<std::uint8_t>(7));
static_assert(rngpp::utility::low_bits_mask_v<4, std::uint8_t> ==
              static_cast<std::uint8_t>(15));
static_assert(rngpp::utility::low_bits_mask_v<5, std::uint8_t> ==
              static_cast<std::uint8_t>(31));
static_assert(rngpp::utility::low_bits_mask_v<6, std::uint8_t> ==
              static_cast<std::uint8_t>(63));
static_assert(rngpp::utility::low_bits_mask_v<7, std::uint8_t> ==
              static_cast<std::uint8_t>(127));
static_assert(rngpp::utility::low_bits_mask_v<8, std::uint8_t> ==
              static_cast<std::uint8_t>(255));

static_assert(rngpp::utility::low_bits_mask_v<1, std::uint16_t> ==
              static_cast<std::uint16_t>(1));
static_assert(rngpp::utility::low_bits_mask_v<2, std::uint16_t> ==
              static_cast<std::uint16_t>(3));
static_assert(rngpp::utility::low_bits_mask_v<3, std::uint16_t> ==
              static_cast<std::uint16_t>(7));
static_assert(rngpp::utility::low_bits_mask_v<4, std::uint16_t> ==
              static_cast<std::uint16_t>(15));
static_assert(rngpp::utility::low_bits_mask_v<5, std::uint16_t> ==
              static_cast<std::uint16_t>(31));
static_assert(rngpp::utility::low_bits_mask_v<6, std::uint16_t> ==
              static_cast<std::uint16_t>(63));
static_assert(rngpp::utility::low_bits_mask_v<7, std::uint16_t> ==
              static_cast<std::uint16_t>(127));
static_assert(rngpp::utility::low_bits_mask_v<8, std::uint16_t> ==
              static_cast<std::uint16_t>(255));
static_assert(rngpp::utility::low_bits_mask_v<9, std::uint16_t> ==
              static_cast<std::uint16_t>(511));
static_assert(rngpp::utility::low_bits_mask_v<10, std::uint16_t> ==
              static_cast<std::uint16_t>(1023));
static_assert(rngpp::utility::low_bits_mask_v<11, std::uint16_t> ==
              static_cast<std::uint16_t>(2047));
static_assert(rngpp::utility::low_bits_mask_v<12, std::uint16_t> ==
              static_cast<std::uint16_t>(4095));
static_assert(rngpp::utility::low_bits_mask_v<13, std::uint16_t> ==
              static_cast<std::uint16_t>(8191));
static_assert(rngpp::utility::low_bits_mask_v<14, std::uint16_t> ==
              static_cast<std::uint16_t>(16383));
static_assert(rngpp::utility::low_bits_mask_v<15, std::uint16_t> ==
              static_cast<std::uint16_t>(32767));
static_assert(rngpp::utility::low_bits_mask_v<16, std::uint16_t> ==
              static_cast<std::uint16_t>(65535));

static_assert(
    rngpp::utility::low_bits_mask_v<std::numeric_limits<std::size_t>::digits> ==
    std::numeric_limits<std::size_t>::max());
