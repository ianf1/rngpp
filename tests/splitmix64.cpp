// Copyright Ian Flint 2019, distributed under the MIT License.

#include "../external/Catch2/single_include/catch2/catch.hpp"

#include <rngpp/splitmix64.hpp>

#include <cstddef>
#include <cstdint>
#include <random>
#include <type_traits>

namespace {

constexpr std::uint64_t seed_validation_value = 18155995546771982973ULL;
constexpr std::uint64_t seed_seq_validation_value = 15161307748162906315ULL;

TEST_CASE("Interface of splitmix64.", "[interface][splitmix64]") {
  constexpr std::size_t test_magic_number = 1903;
  using Generator = rngpp::splitmix64;
  static_assert(Generator::min() == 0);
  static_assert(Generator::max() == ~(static_cast<std::uint64_t>(0)));
  static_assert(std::is_same_v<decltype(Generator{}()), std::uint64_t>);
  SECTION("Equality between different splitmix64_engine classes") {
    rngpp::splitmix64_engine<30, 27, 31, 0xbf58476d1ce4e5b9ULL,
                             0x94d049bb133111ebULL, 33, 33, 33,
                             0xff51afd7ed558ccdULL, 0xc4ceb9fe1a85ec53ULL>
        generator{test_magic_number};
    rngpp::splitmix64_engine<1, 27, 31, 0xbf58476d1ce4e5b9ULL,
                             0x94d049bb133111ebULL, 33, 33, 33,
                             0xff51afd7ed558ccdULL, 0xc4ceb9fe1a85ec53ULL>
        other_generator{test_magic_number};
    REQUIRE_FALSE(generator == other_generator);
  }
  SECTION("splitmix64 seed validation value") {
    Generator generator{};
    for (std::size_t i{0}; i < 9999; ++i) {
      static_cast<void>(generator());
    }
    REQUIRE(generator() == seed_validation_value);
  }
  SECTION("splitmix64 std::seed_seq validation value") {
    std::seed_seq sequence{};
    Generator generator{sequence};
    for (std::size_t i{0}; i < 9999; ++i) {
      static_cast<void>(generator());
    }
    REQUIRE(generator() == seed_seq_validation_value);
  }
  SECTION("splitmix64 discard large amount") {
    const auto value{std::numeric_limits<std::size_t>::max()};
    const auto half{value / 2};
    Generator generator{};
    Generator other_generator{};
    generator.discard(half);
    generator.discard(half);
    generator.discard(value - 2 * half);
    other_generator.discard(value);
    REQUIRE(generator == other_generator);
  }
  SECTION("splitmix64 split") {
    //! The values below have been obtained by executing the Java code below:
    //! import java.util.SplittableRandom;
    //!
    //! public class SplittableRandomExample {
    //!
    //!     public static void main (String[] args) {
    //!         SplittableRandom random = new SplittableRandom(42L);
    //!         SplittableRandom other = random.split();
    //!         for (int i = 0; i < 5; i++) {
    //!             System.out.println(Long.toUnsignedString(other.nextLong()));
    //!         }
    //!     }
    //!
    //! }
    Generator generator{42};
    Generator other{generator.split()};
    REQUIRE(other() == 10935710480581630005ULL);
    REQUIRE(other() == 5410762927873577580ULL);
    REQUIRE(other() == 1172002037136309321ULL);
    REQUIRE(other() == 11491879662333683734ULL);
    REQUIRE(other() == 17016127901299674051ULL);
  }
}

}  // namespace
