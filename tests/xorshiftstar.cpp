// Copyright Ian Flint 2019, distributed under the MIT License.

#include "../external/Catch2/single_include/catch2/catch.hpp"

#include <rngpp/xorshift.hpp>

#include <cstddef>
#include <cstdint>
#include <random>
#include <type_traits>

namespace {

constexpr std::uint32_t seed_validation_value32 = 363308828UL;
constexpr std::uint32_t seed_seq_validation_value32 = 309774694UL;

constexpr std::uint64_t seed_validation_value64 = 15449186730482198630ULL;
constexpr std::uint64_t seed_seq_validation_value64 = 16052948328273607108ULL;

TEST_CASE("Interface of xorshiftstar.", "[interface][xorshiftstar]") {
  constexpr std::size_t test_magic_number = 1304;
  SECTION("xorshiftstar32") {
    using Generator = rngpp::xorshiftstar32;
    static_assert(Generator::min() == 0);
    static_assert(Generator::max() == ~(static_cast<std::uint32_t>(0)));
    static_assert(std::is_same_v<decltype(Generator{}()), std::uint32_t>);
    SECTION("Equality between different xorshiftstar_engine classes") {
      rngpp::xorshiftstarupperbits32a generator{test_magic_number};
      rngpp::xorshiftstarupperbits32b other_generator{test_magic_number};
      REQUIRE_FALSE(generator == other_generator);
    }
    SECTION("seed validation value") {
      Generator generator{};
      for (std::size_t i{0}; i < 9999; ++i) {
        static_cast<void>(generator());
      }
      REQUIRE(generator() == seed_validation_value32);
    }
    SECTION("std::seed_seq validation value") {
      std::seed_seq sequence{};
      Generator generator{sequence};
      for (std::size_t i{0}; i < 9999; ++i) {
        static_cast<void>(generator());
      }
      REQUIRE(generator() == seed_seq_validation_value32);
    }
  }
  SECTION("xorshiftstar64") {
    using Generator = rngpp::xorshiftstar64;
    static_assert(Generator::min() == 1);
    static_assert(Generator::max() == ~(static_cast<std::uint64_t>(0)));
    static_assert(std::is_same_v<decltype(Generator{}()), std::uint64_t>);
    SECTION("seed validation value") {
      Generator generator{};
      for (std::size_t i{0}; i < 9999; ++i) {
        static_cast<void>(generator());
      }
      REQUIRE(generator() == seed_validation_value64);
    }
    SECTION("std::seed_seq validation value") {
      std::seed_seq sequence{};
      Generator generator{sequence};
      for (std::size_t i{0}; i < 9999; ++i) {
        static_cast<void>(generator());
      }
      REQUIRE(generator() == seed_seq_validation_value64);
    }
  }
}

}  // namespace
