// Copyright Ian Flint 2019, distributed under the MIT License.

#include "../external/Catch2/single_include/catch2/catch.hpp"

#include <rngpp/mulberry32.hpp>

#include <cstddef>
#include <cstdint>
#include <limits>
#include <random>
#include <type_traits>

namespace {

constexpr std::uint32_t seed_validation_value = 646087893UL;
constexpr std::uint32_t seed_seq_validation_value = 1666673761UL;

TEST_CASE("Interface of mulberry32.", "[interface][mulberry32]") {
  constexpr std::size_t test_magic_number = 1903;
  using Generator = rngpp::mulberry32;
  static_assert(Generator::min() == 0);
  static_assert(Generator::max() == ~(static_cast<std::uint32_t>(0)));
  static_assert(std::is_same_v<decltype(Generator{}()), std::uint32_t>);
  SECTION("Equality between different mulberry32_engine classes") {
    rngpp::mulberry32_engine<15, 7, 14, 0x6D2B79F5UL, 1, 61> generator{
        test_magic_number};
    rngpp::mulberry32_engine<15, 7, 14, 0x6D2B79F5UL, 1, 1> other_generator{
        test_magic_number};
    REQUIRE_FALSE(generator == other_generator);
  }
  SECTION("mulberry32 seed validation value") {
    Generator generator{};
    for (std::size_t i{0}; i < 9999; ++i) {
      static_cast<void>(generator());
    }
    REQUIRE(static_cast<std::uint32_t>(generator()) == seed_validation_value);
  }
  SECTION("mulberry32 std::seed_seq validation value") {
    std::seed_seq sequence{};
    Generator generator{sequence};
    for (std::size_t i{0}; i < 9999; ++i) {
      static_cast<void>(generator());
    }
    REQUIRE(static_cast<std::uint32_t>(generator()) ==
            seed_seq_validation_value);
  }
  SECTION("mulberry32 discard large amount") {
    constexpr auto value{std::numeric_limits<std::size_t>::max()};
    constexpr auto half{value / 2};
    Generator generator{};
    Generator other_generator{};
    generator.discard(half);
    generator.discard(half);
    generator.discard(value - 2 * half);
    other_generator.discard(value);
    REQUIRE(generator == other_generator);
  }
}

}  // namespace
