// Copyright Ian Flint 2019, distributed under the MIT License.

#include "../external/Catch2/single_include/catch2/catch.hpp"

#include <rngpp/sfc.hpp>

#include <cstddef>
#include <cstdint>
#include <random>
#include <type_traits>

namespace {

constexpr std::uint32_t seed_validation_value32 = 1279691267UL;
constexpr std::uint32_t seed_seq_validation_value32 = 1251357140UL;

constexpr std::uint64_t seed_validation_value64 = 12265472885513603174ULL;
constexpr std::uint64_t seed_seq_validation_value64 = 15504702023275614161ULL;

TEST_CASE("Interface of sfc.", "[interface][sfc]") {
  constexpr std::size_t test_magic_number = 9789;
  SECTION("Equality between different sfc classes") {
    rngpp::sfc32 generator{test_magic_number};
    rngpp::sfc64 other_generator{test_magic_number};
    REQUIRE_FALSE(generator == other_generator);
  }
  SECTION("sfc32") {
    using Generator = rngpp::sfc32;
    static_assert(Generator::min() == 0);
    static_assert(Generator::max() == ~(static_cast<std::uint32_t>(0)));
    static_assert(std::is_same_v<decltype(Generator{}()), std::uint32_t>);
    SECTION("seed validation value") {
      Generator generator{};
      for (std::size_t i{0}; i < 9999; ++i) {
        static_cast<void>(generator());
      }
      REQUIRE(generator() == seed_validation_value32);
    }
    SECTION("std::seed_seq validation value") {
      std::seed_seq sequence{};
      Generator generator{sequence};
      for (std::size_t i{0}; i < 9999; ++i) {
        static_cast<void>(generator());
      }
      REQUIRE(generator() == seed_seq_validation_value32);
    }
  }
  SECTION("sfc64") {
    using Generator = rngpp::sfc64;
    static_assert(Generator::min() == 0);
    static_assert(Generator::max() == ~(static_cast<std::uint64_t>(0)));
    static_assert(std::is_same_v<decltype(Generator{}()), std::uint64_t>);
    SECTION("seed validation value") {
      Generator generator{};
      for (std::size_t i{0}; i < 9999; ++i) {
        static_cast<void>(generator());
      }
      REQUIRE(generator() == seed_validation_value64);
    }
    SECTION("std::seed_seq validation value") {
      std::seed_seq sequence{};
      Generator generator{sequence};
      for (std::size_t i{0}; i < 9999; ++i) {
        static_cast<void>(generator());
      }
      REQUIRE(generator() == seed_seq_validation_value64);
    }
  }
}

}  // namespace
