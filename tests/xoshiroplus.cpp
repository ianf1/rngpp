// Copyright Ian Flint 2019, distributed under the MIT License.

#include "../external/Catch2/single_include/catch2/catch.hpp"

#include <rngpp/xoshiro.hpp>

#include <cstddef>
#include <cstdint>
#include <random>
#include <type_traits>

namespace {

constexpr std::uint32_t seed_validation_value32 = 366695368UL;
constexpr std::uint32_t seed_seq_validation_value32 = 1633378697UL;
constexpr std::uint32_t jump_validation_value32 = 3821071956UL;

constexpr std::uint64_t seed_validation_value64 = 3744384939819984536ULL;
constexpr std::uint64_t seed_seq_validation_value64 = 12332365641367551134ULL;
constexpr std::uint64_t jump_validation_value64 = 10865428133116009533ULL;
constexpr std::uint64_t long_jump_validation_value64 = 10460559529585360936ULL;

TEST_CASE("Interface of xoshiroplus.", "[interface][xoshiroplus]") {
  constexpr std::size_t test_magic_number = 9789;
  SECTION("Equality between different xoshiroplus classes") {
    rngpp::xoshiroplus32 generator{test_magic_number};
    rngpp::xoshiroplus64 other_generator{test_magic_number};
    REQUIRE_FALSE(generator == other_generator);
  }
  SECTION("xoshirostarstar32") {
    using Generator = rngpp::xoshiroplus32;
    static_assert(Generator::min() == 0);
    static_assert(Generator::max() == ~(static_cast<std::uint32_t>(0)));
    static_assert(std::is_same_v<decltype(Generator{}()), std::uint32_t>);
    SECTION("seed validation value") {
      Generator generator{};
      for (std::size_t i{0}; i < 9999; ++i) {
        static_cast<void>(generator());
      }
      REQUIRE(generator() == seed_validation_value32);
    }
    SECTION("std::seed_seq validation value") {
      std::seed_seq sequence{};
      Generator generator{sequence};
      for (std::size_t i{0}; i < 9999; ++i) {
        static_cast<void>(generator());
      }
      REQUIRE(generator() == seed_seq_validation_value32);
    }
    SECTION("jump validation value") {
      Generator generator{};
      generator.jump();
      for (std::size_t i{0}; i < 9999; ++i) {
        static_cast<void>(generator());
      }
      REQUIRE(generator() == jump_validation_value32);
    }
  }
  SECTION("xoshiroplus64") {
    using Generator = rngpp::xoshiroplus64;
    static_assert(Generator::min() == 0);
    static_assert(Generator::max() == ~(static_cast<std::uint64_t>(0)));
    static_assert(std::is_same_v<decltype(Generator{}()), std::uint64_t>);
    SECTION("seed validation value") {
      Generator generator{};
      for (std::size_t i{0}; i < 9999; ++i) {
        static_cast<void>(generator());
      }
      REQUIRE(generator() == seed_validation_value64);
    }
    SECTION("std::seed_seq validation value") {
      std::seed_seq sequence{};
      Generator generator{sequence};
      for (std::size_t i{0}; i < 9999; ++i) {
        static_cast<void>(generator());
      }
      REQUIRE(generator() == seed_seq_validation_value64);
    }
    SECTION("jump validation value") {
      Generator generator{};
      generator.jump();
      for (std::size_t i{0}; i < 9999; ++i) {
        static_cast<void>(generator());
      }
      REQUIRE(generator() == jump_validation_value64);
    }
    SECTION("long-jump validation value") {
      Generator generator{};
      generator.long_jump();
      for (std::size_t i{0}; i < 9999; ++i) {
        static_cast<void>(generator());
      }
      REQUIRE(generator() == long_jump_validation_value64);
    }
  }
}

}  // namespace
