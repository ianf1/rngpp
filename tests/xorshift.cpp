// Copyright Ian Flint 2019, distributed under the MIT License.

#include "../external/Catch2/single_include/catch2/catch.hpp"

#include <rngpp/xorshift.hpp>

#include <cstddef>
#include <cstdint>
#include <random>
#include <type_traits>

namespace {

constexpr std::uint32_t seed_validation_value32 = 261659008UL;
constexpr std::uint32_t seed_seq_validation_value32 = 2167099490UL;

constexpr std::uint64_t seed_validation_value64 = 16670172405854946724ULL;
constexpr std::uint64_t seed_seq_validation_value64 = 13825447156384741639ULL;

TEST_CASE("Interface of xorshift.", "[interface][xorshift]") {
  constexpr std::size_t test_magic_number = 9789;
  SECTION("Equality between different xorshift classes") {
    rngpp::xorshift32 generator{test_magic_number};
    rngpp::xorshift64 other_generator{test_magic_number};
    REQUIRE_FALSE(generator == other_generator);
  }
  SECTION("xorshift32") {
    using Generator = rngpp::xorshift32;
    static_assert(Generator::min() == 1);
    static_assert(Generator::max() == ~(static_cast<std::uint32_t>(0)));
    static_assert(std::is_same_v<decltype(Generator{}()), std::uint32_t>);
    SECTION("seed validation value") {
      Generator generator{};
      for (std::size_t i{0}; i < 9999; ++i) {
        static_cast<void>(generator());
      }
      REQUIRE(generator() == seed_validation_value32);
    }
    SECTION("std::seed_seq validation value") {
      std::seed_seq sequence{};
      Generator generator{sequence};
      for (std::size_t i{0}; i < 9999; ++i) {
        static_cast<void>(generator());
      }
      REQUIRE(generator() == seed_seq_validation_value32);
    }
  }
  SECTION("xorshift64") {
    using Generator = rngpp::xorshift64;
    static_assert(Generator::min() == 1);
    static_assert(Generator::max() == ~(static_cast<std::uint64_t>(0)));
    static_assert(std::is_same_v<decltype(Generator{}()), std::uint64_t>);
    SECTION("seed validation value") {
      Generator generator{};
      for (std::size_t i{0}; i < 9999; ++i) {
        static_cast<void>(generator());
      }
      REQUIRE(generator() == seed_validation_value64);
    }
    SECTION("std::seed_seq validation value") {
      std::seed_seq sequence{};
      Generator generator{sequence};
      for (std::size_t i{0}; i < 9999; ++i) {
        static_cast<void>(generator());
      }
      REQUIRE(generator() == seed_seq_validation_value64);
    }
  }
}

}  // namespace
